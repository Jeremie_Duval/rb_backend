#!/bin/bash
set -e

# setup ros environment
#source "/home/jduval/ROS_PROJECT/devel/setup.bash"
source "/home/sirsteward/ROS_PROJECT/devel/setup.bash"
#source "/root/sirSteward_ws/devel/setup.bash"
export ROSCONSOLE_FORMAT='[${severity}][${time:%x %r}][${node}] line: ${line} [function:${function}]: ${message}'
roslaunch controller_nodes controller.launch
exec "$@"

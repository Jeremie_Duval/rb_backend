import os
import time

import rospy
from ui_user_nodes.msg import pantheraGoalsList, pantheraGoals
rospy.init_node(os.path.splitext(os.path.basename(__file__))[0])

ui_goals1 = pantheraGoals()
ui_goals2 = pantheraGoals()
ui_goals3 = pantheraGoals()

ui_goals1.goal_id = 'test1'
ui_goals1.goal_name = 'coucou'

ui_goals2.goal_id = 'test2'
ui_goals2.goal_name = 'holaSiniorita'

ui_goals3.goal_id = 'test3'
ui_goals3.goal_name = 'Mi Amigo!!'

listofgoals = pantheraGoalsList()
listofgoals.list_of_goals = [ui_goals1,ui_goals2,ui_goals3]

pub = rospy.Publisher('panth_list_goals', pantheraGoalsList, queue_size=10)
time.sleep(5)
pub.publish(listofgoals)

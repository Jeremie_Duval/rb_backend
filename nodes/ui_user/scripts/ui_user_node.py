#!/usr/bin/env python3.8

import os
import rospy
import signal
import traceback
from multiprocessing import Process
from std_msgs.msg import String
from ui_client.panthera_ui import PantheraUI
from ui_client.digifab_ui import DigifabUI
from ui_client.palais_des_congres_ui import PalaisDesCongresUI

"""

"""
# ros topic names
UI_GENERAL_COMMANDS = rospy.get_param('ui_general_command', 'ui_general_cmd')
UI_STATUS_PUBLISHER = rospy.get_param('ui_user_state', 'ui_user_state')


class UiUserNode:
    def __init__(self):
        # ui general variables
        self.ui = None
        self.ui_process_list = []
        self.ui_type = rospy.get_param('ui_user_type', 'panthera')

    def begin(self):
        try:
            try:  # Pour régler le problème de mapping à partir d'un launch
                del rospy.names.get_mappings()['__name']
            except KeyError:
                pass
            if self.ui_type == 'Panthera':
                panth_ui_process = Process(target=self.panthera_process)
                self.ui_process_list.append(panth_ui_process)

            if self.ui_type == 'Digifab':
                digifab_ui_process = Process(target=self.digifab_process)
                self.ui_process_list.append(digifab_ui_process)

            elif self.ui_type == 'Palais_des_congres':
                palais_ui_process = Process(target=self.palais_des_congres_process)
                self.ui_process_list.append(palais_ui_process)
            else:
                rospy.logerr('----- [UI_USER] ***** User ui type not available ***** -----')
                rospy.signal_shutdown("User ui type not available")

            # starting the the web applications server of the ui clients
            # for the moment, TAG only have 2 cores, so there should be only one client
            for ui_process in self.ui_process_list:
                ui_process.start()
            rospy.loginfo("----- [UI_USER] ***** All Uis process have been started ***** -----")

        except Exception:
            rospy.logerr(f'----- [UI_USER] ***** Initialisation failed: {traceback.format_exc()} ***** -----')
            rospy.signal_shutdown("Initialisation Failed")

    @staticmethod
    def panthera_process():
        ip_address = rospy.get_param('localhost_ip', '127.0.0.1')
        port = rospy.get_param('panthera_port', 8000)
        ui = PantheraUI(ip_address, port, UI_GENERAL_COMMANDS, UI_STATUS_PUBLISHER)
        ui.connect_ui()

    @staticmethod
    def digifab_process():
        ip_address = rospy.get_param('localhost_ip', '127.0.0.1')
        port = rospy.get_param('digifab_port', 8000)
        ui = DigifabUI(ip_address, port, UI_GENERAL_COMMANDS, UI_STATUS_PUBLISHER)
        ui.connect_ui()

    @staticmethod
    def palais_des_congres_process():
        ip_address = rospy.get_param('localhost_ip', '127.0.0.1')
        port = rospy.get_param('Palais_des_congres_port', 8000)
        ui = PalaisDesCongresUI(ip_address, port, UI_GENERAL_COMMANDS, UI_STATUS_PUBLISHER)
        ui.connect_ui()

    def node_shutdown(self):
        rospy.loginfo("----- [UI_USER] ***** Node is shutting down ***** -----")
        for ui_process in self.ui_process_list:
            ui_process.kill()
            ui_process.close()


if __name__ == "__main__":
    try:
        node = UiUserNode()
        node.begin()
        rospy.init_node(os.path.splitext(os.path.basename(__file__))[0])
        rospy.on_shutdown(node.node_shutdown)
        # TODO: Find a way to use rospy.spin() to shutdown the node in first try
        # You need to find a way to not block with web.run_app
        rospy.spin()
        # signal.signal(signal.SIGINT, node.signal_handler)
    except Exception:
        #print(traceback.format_exc())
        rospy.logerr(traceback.format_exc())
#!/usr/bin/env python

import os
import time

import rospy
import traceback
from std_msgs.msg import String
from comm_protocols_drivers.socket_listener import SocketListener

"""

"""
# IP_ADDRESS = "192.168.3.1"
# IP_ADDRESS = "192.168.50.152"
IP_ADDRESS = rospy.get_param('omron_socket_ip', '192.168.3.1')
PORT = 7179

STATUS_PUB = rospy.get_param('omron_status', 'omron_base_status')
READABLE_STATUS_PUB = rospy.get_param('omron_read_status', 'omron_base_humanReadableStatus')
BATTERY_PUB = rospy.get_param('omron_battery', 'omron_base_SoC')
LOCATION_PUB = rospy.get_param('omron_location', 'omron_base_location')
LOCALIZATION_SCORE_PUB = rospy.get_param('omron_localization', 'omron_base_localisationScore')
TEMPERATURE_PUB = rospy.get_param('omron_temperature', 'omron_base_temperature')
OTHER_INFO_PUB = rospy.get_param('omron_base_other', 'omron_base_other')


class OmronStatusPublisher:
    def __init__(self):
        ip_address = rospy.get_param("omron_socket_ip", "192.168.3.1")
        port = rospy.get_param("socket_port", 7179)

        self.socket_listener = SocketListener(ip_address, port)

        self.status_publisher = rospy.Publisher(STATUS_PUB, String, queue_size=5)
        self.human_status_publisher = rospy.Publisher(READABLE_STATUS_PUB, String, queue_size=5)
        self.soc_publisher = rospy.Publisher(BATTERY_PUB, String, queue_size=5)
        self.location_publisher = rospy.Publisher(LOCATION_PUB, String, queue_size=5)
        self.localization_score_publisher = rospy.Publisher(LOCALIZATION_SCORE_PUB, String, queue_size=5)
        self.temperature_publisher = rospy.Publisher(TEMPERATURE_PUB, String, queue_size=5)
        self.other_informations_publisher = rospy.Publisher(OTHER_INFO_PUB, String, queue_size=5)

    def analyse_publish_data(self):
        while not rospy.is_shutdown():
            response = self.socket_listener.get_sorted_data()
            if response:
                for key, value in response.items():
                    if key == 'Status':
                        self.status_publisher.publish(value)
                    elif key == 'StateOfCharge':
                        self.soc_publisher.publish(value)
                    elif key == 'Location':
                        self.location_publisher.publish(value)
                    elif key == 'LocalizationScore':
                        self.localization_score_publisher.publish(value)
                    elif key == 'Temperature':
                        self.temperature_publisher.publish(value)
                    elif key == 'ExtendedStatusForHumans':
                        self.human_status_publisher.publish(value)
                    else:
                        self.other_informations_publisher.publish(key + response.get(key, ""))
            else:
                rospy.logwarn("----- [SOCKET] ***** Socket connexion has been lost will try to reconnect ***** -----")

    def begin(self):
        if self.socket_listener.connect():
            rospy.loginfo("----- [SOCKET] ***** Omron base can now update it's status ***** -----")
        self.analyse_publish_data()

    def node_shutdown(self):
        """
        This method informs the developper about the shutdown of this node
        """
        # rospy.loginfo(f"{self.__class__.__name__} is shutting down")
        self.socket_listener.close()


if __name__ == "__main__":
    try:
        rospy.init_node(os.path.splitext(os.path.basename(__file__))[0])
        node = OmronStatusPublisher()
        rospy.on_shutdown(node.node_shutdown)
        node.begin()
        rospy.spin()
    except Exception:
        rospy.logerr(traceback.format_exc())

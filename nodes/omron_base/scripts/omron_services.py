#!/usr/bin/env python
import os
import csv
import glob
import time
import rospy
import datetime
import actionlib
import threading
import traceback
from std_msgs.msg import String, Bool, Empty
from comm_protocols_drivers.telnet_client import TelnetOmron
from omron_base_nodes.srv import arclSrv, arclSrvRequest, arclSrvResponse
from omron_base_nodes.msg import gotoActionAction, gotoActionGoal, gotoActionResult, gotoActionFeedback

# from ros_integration.sir_steward_common.comm_protocols_drivers import TelnetOmron

# todo: faire en sorte que pycharm puisse trouver les références de ros suite à source ./devel/setup.bash

"""

"""
# ros topic names
ESTOP_PUBLISHER = rospy.get_param('estop_publisher', 'estop')
ARCL_PUBLISHER = rospy.get_param('arcl_publisher', 'arcl_messages')
ARCL_SUBSCRIBER = rospy.get_param('arcl_subscriber', 'arcl_commands')
ARCL_SERVICES = rospy.get_param('arcl_services', 'arcl_services')
ARCL_GOTO_ACTION = rospy.get_param('arcl_goto_action', 'arcl_goto_action')

NO_ANSWER_TIMEOUT = 1
RETRY_ACTION_GOTO = 3

COMMANDS = {
    'gotopoint': {'start_mess': 'going to',
                  'end_mess': ['arrived at', 'interrupted', 'failed going to goal', 'cannot find path', 'robot lost']},
    'goto': {'start_mess': 'going to',
             'end_mess': ['arrived at', 'interrupted', 'failed going to goal', 'cannot find path', 'robot lost']},
    'dotask gotostraight': {'start_mess': 'doing task gotostraight',
                            'end_mess': ['completed doing task gotostraight', 'interrupted: doing task gotostraight',
                                         'failed doing task gotostraight']},
    'undock': {'start_mess': 'dockingstate: undocking', 'end_mess': ['dockingstate: undocked'], 'no_answer': True},
    'dock': {'start_mess': 'dockingstate: docking',
             'end_mess': ['dockingstate: docked', 'going to', 'doing task gotostraight'], 'no_answer': True},
    'stop': {'start_mess': 'stopping', 'end_mess': ['stopped'], 'no_answer': True},
    'status': {'start_mess': 'extendedstatusforhumans', 'end_mess': ['temperature']},
    'play': {'start_mess': '', 'end_mess': ['playing']},
    'say': {'start_mess': '', 'end_mess': ['saying']},
    'querymotors': {'start_mess': '', 'end_mess': ['motors', 'estop pressed', 'estop relieved']},
    'mapobjectlist': {'start_mess': 'mapobjectlist:', 'end_mess': ['end of mapobjectlist']},
    'getconfigsectionlist': {'start_mess': 'getconfigsectionlist', 'end_mess': ['endofgetconfigsectionlist']},
    'mapobjectinfo': {'start_mess': 'mapobjectinfo:', 'end_mess': ['end of mapobjectinfo']},
    'getgoals': {'start_mess': 'goal:', 'end_mess': ['end of goals']},
    'localizetopoint': {'start_mess': '', 'end_mess': ['localized to point']},
    'enablemotors': {'start_mess': 'doing task', 'end_mess': ['motors enabled']},
    'arclsendtext': {'start_mess': '', 'end_mess': ['']},
    'shutdown': {'start_mess': '', 'end_mess': ['']},
    'restart': {'start_mess': '', 'end_mess': ['']},
}
COMMAND_ERROR = ['Unknown command', 'CommandErrorDescription', 'SetupError']


class OmronServices:
    def __init__(self):
        ip_address = rospy.get_param("omron_telnet_ip", '192.168.50.96')
        port = rospy.get_param("telnet_port", '7171')
        password = rospy.get_param('arcl_passwd', 'Amvic145')

        self.telnet_client = TelnetOmron(ip_address, port, password)
        self.__send_lock = threading.Lock()
        self.__service_lock = threading.Lock()
        self.__action_lock = threading.Lock()
        self.read_thread = threading.Thread(target=self.__read_loop)

        # Ros publisher initialisation
        self.estop_pub: rospy.Publisher = None
        self.arcl_publish: rospy.Publisher = None

        # Ros subscriber/services/actions initialise without callbacks
        self.arcl_subscribe: rospy.Subscriber = None
        self.arcl_service: rospy.Service = None
        self.arcl_goto_action: actionlib.SimpleActionServer = None

        self.service_command = {}
        self.service_answer = ""
        self.no_answer_clock_service = None

        self.action_command_info = {}
        self.action_command_details = []
        self.action_answer = ""
        self.no_answer_clock_action = None

        # Flags
        self.__is_closing = False
        self.__service_on = False
        self.__action_start = False
        self.estop_pressed = False

        # Audio - handles time
        # self.play_service = None
        # self.__analyse_audio_file()
        # self.audio_file_dict = []

    def begin(self):
        try:
            if self.telnet_client.connect():
                self.estop_pub = rospy.Publisher(ESTOP_PUBLISHER, Bool, queue_size=5)
                self.arcl_publish = rospy.Publisher(ARCL_PUBLISHER, String, queue_size=10)
                # Initialisation with Callback
                self.arcl_subscribe = rospy.Subscriber(ARCL_SUBSCRIBER, String, callback=self.__sub_cb, queue_size=10)
                self.arcl_service = rospy.Service(ARCL_SERVICES, arclSrv, self.__service_cb)
                self.arcl_goto_action = actionlib.SimpleActionServer(ARCL_GOTO_ACTION, gotoActionAction,
                                                                     self.__goal_action_cb, auto_start=False)
                self.arcl_goto_action.register_preempt_callback(self.__interrupt_action_cb)
                # self.play_service = rospy.Service('play_audio_service', playAudioFile, self.__play_cb)

                self.arcl_goto_action.start()
                self.read_thread.start()
                # Update estop value at the beginning of the node
                rospy.loginfo("----- [OMRON] ***** Omron base is ready to receive commands ***** -----")
                self.update_estop()
            else:
                rospy.logerr("----- [OMRON] ***** Telnet did not connect ***** -----")

        except Exception:
            rospy.logerr(f'----- [OMRON] ***** Initialisation failed: {traceback.format_exc()} ***** -----')
            rospy.signal_shutdown("Initialisation Failed")

    def send_cmd(self, command):
        self.__send_lock.acquire()

        self.arcl_publish.publish(command)
        self.telnet_client.send(command)
        rospy.loginfo(f"ARCL --> {command}")

        self.__send_lock.release()

    def update_estop(self):
        self.send_cmd('querymotors')

    def node_shutdown(self):
        """
        This method informs the developper about the shutdown of this node
        """
        rospy.loginfo("node shutting down")
        self.__is_closing = True
        try:
            self.telnet_client.close()
            if self.read_thread.is_alive():
                self.read_thread.join()
                self.send_cmd('stop')
                self.arcl_service.shutdown()
                self.arcl_publish.unregister()
                self.arcl_subscribe.unregister()
        except Exception:
            rospy.logerr(f'----- [OMRON] ***** {traceback.format_exc()} ***** -----')
        finally:
            self.__is_closing = False

    def __read_loop(self):
        try:
            while not rospy.is_shutdown() and not self.__is_closing:
                data = self.telnet_client.read_line()
                if data:
                    rospy.loginfo(f"ARCL <-- {data}")
                    self.arcl_publish.publish(data)
                    if "estop" in data:
                        self.__handles_estop(data)
                    if self.__service_on:
                        self.__get_service_answer(data)
                    if self.arcl_goto_action.is_active():
                        self.__get_action_answer(data)
                else:
                    self.__handles_no_answer_commands()

        except Exception:
            rospy.logerr(f'----- [OMRON] ***** {traceback.format_exc()} ***** -----')

    def __sub_cb(self, command):
        self.send_cmd(command.data)

    def __service_cb(self, message: arclSrvRequest):
        command_with_no_args = message.command.split()[0].lower()
        dict_command = COMMANDS.get(command_with_no_args, None)
        # returning None from a service means it failed
        answer = None
        if dict_command is None:
            rospy.logwarn(f"----- [OMRON] ***** This command is not program: {command_with_no_args} ***** -----")
        else:
            self.__service_lock.acquire()

            self.service_command = dict_command
            self.__service_on = True
            self.send_cmd(message.command.lower())
            if dict_command.get('no_answer', False):
                self.no_answer_clock_service = datetime.datetime.now()

            while not rospy.is_shutdown() and self.__service_on and not self.__is_closing:
                time.sleep(0.5)

            answer = arclSrvResponse()
            answer.answer = self.service_answer
            self.service_answer = ""
            self.service_command = {}

            self.__service_lock.release()

        return answer

    def __goal_action_cb(self, goal: gotoActionGoal):
        # TODO: There is a problem with goals with capital_letters for goto command -- fixed
        command = ""
        result = gotoActionResult()
        feedback = gotoActionFeedback()
        need_reverse = False
        if not self.estop_pressed:
            if goal.command == 'goto' or goal.command == 'dotask gotostraight':
                command = f'{goal.command} {goal.goal}'
                self.action_command_details = [goal.goal]
                if goal.reverse:
                    need_reverse = True
            elif goal.command == 'gotoPoint':
                print(f'This is our goal to reach with gotoPoint command : {goal}')
                heading = goal.heading
                if goal.reverse:
                    heading = reverse_heading(heading)
                command = f'{goal.command} {goal.x} {goal.y} {heading}'
                self.action_command_details = [f'{goal.x} {goal.y}']
            elif goal.command in ['dock', 'undock']:
                command = goal.command
            else:
                rospy.logwarn(f'----- [OMRON] ***** This command is not programmed for this action Server: '
                              f'{goal.command} ***** -----')
                result.details = 'Command not programmed'
                self.arcl_goto_action.set_aborted(result)
            self.__action_lock.acquire()
            self.action_command_info = COMMANDS.get(goal.command.lower(), {})
            if self.action_command_info.get('no_answer', False):
                self.no_answer_clock_action = datetime.datetime.now()
            self.send_cmd(command)
            retry = 0

            while self.arcl_goto_action.is_active():
                if rospy.is_shutdown() or self.__is_closing:
                    result.details = 'Omron node has been shutdown'
                    self.arcl_goto_action.set_aborted(result)
                if self.action_answer:
                    result.details = self.action_answer
                    if any(succ_mess in self.action_answer for succ_mess in
                           ['arrived at', 'completed doing task', 'dockingstate: docked',
                            'dockingstate: undocked']):
                        rospy.loginfo('----- [OMRON] ***** Action is successfully done ***** -----')
                        if need_reverse:
                            new_pos = self.__get_reverse_position()
                            self.action_command_info = COMMANDS.get('gotopoint')
                            command = f"gotopoint {new_pos['x']} {new_pos['y']} {new_pos['heading']}"
                            need_reverse = False
                            self.action_answer = ""
                            self.send_cmd(command)
                            feedback.feedback = 'reversing robot'
                            self.arcl_goto_action.publish_feedback(feedback)
                        else:
                            self.arcl_goto_action.set_succeeded(result)
                    else:
                        if any(failed_mess in self.action_answer for failed_mess in
                               ['failed going to goal', 'failed doing task']):
                            # TODO: Could implement a sound when this situation happens
                            if retry < RETRY_ACTION_GOTO:
                                rospy.loginfo('----- [OMRON] ***** Retrying going to goal ***** -----')
                                retry += 1
                                self.action_answer = ""
                                self.send_cmd(command)
                                feedback.feedback = 'retrying going to goal'
                                self.arcl_goto_action.publish_feedback(feedback)
                                continue
                        rospy.loginfo('----- [OMRON] ***** Action is aborted ***** -----')
                        self.arcl_goto_action.set_aborted(result)
                time.sleep(0.5)

            self.action_command_info = {}
            self.action_answer = ""
            self.no_answer_clock_action = None
            self.__action_start = False
            self.__action_lock.release()
        else:
            rospy.loginfo('----- [ESTOP] ***** Goal aborted because estop pressed ***** -----')
            result.details = 'estop pressed'
            self.arcl_goto_action.set_aborted(result)

    def __interrupt_action_cb(self):
        rospy.loginfo('----- [OMRON] ***** Goto action is being interrupted ***** -----')
        result = gotoActionResult()
        result.details = 'Action interrupted'
        self.send_cmd('stop')
        self.arcl_goto_action.set_preempted(result)

    def __handles_estop(self, data):
        if 'pressed' in data:
            self.estop_pub.publish(True)
            self.estop_pressed = True
        elif 'relieve' in data:
            self.estop_pub.publish(False)
            self.estop_pressed = False
        else:
            rospy.logwarn(f'----- [ESTOP] ***** Info about estop do not concern release or pressed : '
                          f'{data} ***** -----')

    def __handles_no_answer_commands(self):
        # To handle command with possibly no answer like dock when robot already docked
        if self.__service_on:
            if self.no_answer_clock_service is not None:
                if self.service_answer:
                    self.no_answer_clock_service = None
                elif (datetime.datetime.now() - self.no_answer_clock_service).seconds > NO_ANSWER_TIMEOUT:
                    self.no_answer_clock_service = None
                    self.service_answer = "Already done"
                    self.__service_on = False
        # same for action
        if self.arcl_goto_action.is_active():
            if self.no_answer_clock_action is not None:
                if self.__action_start:
                    self.no_answer_clock_action = None
                elif (datetime.datetime.now() - self.no_answer_clock_action).seconds > NO_ANSWER_TIMEOUT:
                    self.no_answer_clock_action = None
                    result = gotoActionResult()
                    result.details = 'Already done'
                    self.arcl_goto_action.set_succeeded(result)

    def __get_service_answer(self, data):
        start_mess = self.service_command.get('start_mess')
        if start_mess and start_mess in data:
            self.service_answer += data
        elif any(end_mess in data for end_mess in self.service_command.get('end_mess')):
            self.service_answer += data
            self.__service_on = False
        elif self.service_answer:
            self.service_answer += data
        elif any(err_mess in data for err_mess in COMMAND_ERROR):
            self.service_answer += data
            self.__service_on = False

    def __get_action_answer(self, data):
        start_mess = self.action_command_info.get('start_mess')
        if start_mess and start_mess in data:
            self.__action_start = True
        if self.__action_start:
            if any(end_mess in data for end_mess in self.action_command_info.get('end_mess')):
                for command_detail in self.action_command_details:
                    command_detail_lower = command_detail.lower()
                    if any(goal_mess in data for goal_mess in command_detail_lower):
                        if not self.action_answer:
                            self.action_answer += data
                            rospy.logwarn(f'----- [OMRON] ***** Action answer : {self.action_answer} ***** -----')
        elif any(err_mess in data for err_mess in COMMAND_ERROR):
            self.action_answer += data

    @staticmethod
    def __minutes_to_seconds(minutes):
        minutes_list = (minutes.split(':'))
        if len(minutes_list) == 2:
            seconds = int(minutes_list[0]) * 60 + int(minutes_list[1])
        elif len(minutes_list) == 1:
            seconds = int(minutes_list[0])
        else:
            rospy.logerr("----- [AUDIO] ***** Duration of audio file is too long ***** -----")
            return False
        return seconds

    @staticmethod
    def __get_reverse_position():
        service_client = rospy.ServiceProxy(ARCL_SERVICES, arclSrv)
        # TODO: find a more efficient way to know the geo position of a mobileplanner goal
        status = service_client.call('status')
        dict_status = parse_status(status.answer)
        location = dict_status.get('location')
        x, y, heading = location.split()
        heading = str(reverse_heading(int(heading)))
        new_pos = {'x': x, 'y': y, 'heading': heading}
        return new_pos


def parse_status(status):
    dict_status = {}
    for line in status.splitlines():
        tuple = line.split(":")
        key = tuple[0]
        value = tuple[1]
        dict_status[key] = value
    return dict_status


def reverse_heading(heading):
    if heading > 0:
        heading -= 180
    else:
        heading += 180
    return heading


if __name__ == "__main__":
    try:
        rospy.init_node(os.path.splitext(os.path.basename(__file__))[0])
        node = OmronServices()
        rospy.on_shutdown(node.node_shutdown)
        node.begin()
        rospy.spin()
        # signal.signal(signal.SIGINT, node.signal_handler)
    except Exception:
        rospy.logerr(traceback.format_exc())

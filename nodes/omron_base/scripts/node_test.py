#!/usr/bin/env python

import os
import time

import actionlib
import rospy
import traceback
from std_msgs.msg import String
from comm_protocols_drivers.socket_listener import SocketListener
from omron_base_nodes.msg import gotoActionAction, gotoActionGoal, gotoActionResult, gotoActionFeedback


class TestRosIP:
    def __init__(self):
        self.action_client = actionlib.SimpleActionClient('arcl_goto_action', gotoActionAction)

    def begin(self):
        while not rospy.is_shutdown():
            action = gotoActionGoal()
            action.command = 'goto'
            action.goal = 'start'
            time.sleep(2)
            print('sending command')
            result = self.action_client.send_goal_and_wait(action)
            print(result)
            print(self.action_client.get_result())
            print(self.action_client.get_state())
            # print(self.action_client.wait_for_result())
            # print(self.action_client.get_result())
            time.sleep(3)

    def node_shutdown(self):
        rospy.loginfo('Test de ROS_IP terminé')

if __name__ == "__main__":
    try:
        rospy.init_node(os.path.splitext(os.path.basename(__file__))[0])
        node = TestRosIP()
        rospy.on_shutdown(node.node_shutdown)
        node.begin()
        rospy.spin()
    except Exception:
        rospy.logerr(traceback.format_exc())

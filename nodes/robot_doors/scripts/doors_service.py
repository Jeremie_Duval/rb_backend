#!/usr/bin/env python

import os
import rospy
import functools
import traceback
import actionlib
from std_msgs.msg import String
from robot_door_drivers.serial_doors import SerialDoors
from robot_doors_node.msg import doorCalibrationActionAction, doorCalibrationActionGoal, doorCalibrationActionResult
from robot_doors_node.msg import doorCalibrationActionFeedback
from robot_doors_node.msg import doorActionAction, doorActionGoal, doorActionResult, doorActionFeedback
from robot_doors_node.srv import doorSrvTorqueAllowed, doorSrvTorqueAllowedRequest

"""

"""
# ros topic names
DOOR_PUBLISHER = rospy.get_param('door_logs', 'door_logs')
DOOR_TYPE = rospy.get_param('door_type', 'motorized')
OPEN_DOOR_ACTION = rospy.get_param('open_door_action', 'open_door_action')
CLOSE_DOOR_ACTION = rospy.get_param('close_door_action', 'close_door_action')
DOOR_CALIBRATION_ACTION = rospy.get_param('door_calibration_action', 'door_calibration_action')
TORQUE_ALLOWED_SERVICE = rospy.get_param('door_torque_allowed_service', 'door_torque_allowed_service')


class DoorServices:
    def _service_function_decorator(function):
        @functools.wraps(function)
        def wrapper(self, *args):
            self.door_logs.publish(f"{args}")
            answer = [False, ""]
            try:
                function(self, *args)
                answer[0] = True
            except ValueError as val_err:
                rospy.logerr(f'----- [DOOR] ***** {str(val_err)} ***** -----')
                answer[1] = str(val_err)
            finally:
                self.door_logs.publish(f" Success: {answer[0]}, details:{answer[1]}")
                return answer

        return wrapper

    def __init__(self):
        self.door: SerialDoors = None
        self.door = self.__initialize_door()

        # Topics
        self.door_logs: rospy.Publisher = None

        # Services
        self.torque_allowed_service: rospy.Service = None

        # Actions
        self.door_calibration_action: actionlib.SimpleActionServer = None
        self.open_door_action: actionlib.SimpleActionServer = None
        self.close_door_action: actionlib.SimpleActionServer = None

    def begin(self):
        try:
            if self.door.connect():
                # Initialisation with Callback
                self.door_logs = rospy.Publisher(DOOR_PUBLISHER, String, queue_size=5)
                self.__start_services_and_actions()

            else:
                rospy.logerr("----- [DOOR] ***** Door did not connect ***** -----")

        except Exception:
            rospy.logerr(f'----- [DOOR] ***** Initialisation failed: {traceback.format_exc()} ***** -----')
            rospy.signal_shutdown("Initialisation Failed")

    def __start_services_and_actions(self):

        if DOOR_TYPE == 'motorized':
            self.open_door_action = actionlib.SimpleActionServer(OPEN_DOOR_ACTION, doorActionAction,
                                                                 self.__open_door_action_cb, auto_start=False)
            self.open_door_action.register_preempt_callback(self.__open_door_interrupt_cb)
            self.open_door_action.start()

            self.close_door_action = actionlib.SimpleActionServer(CLOSE_DOOR_ACTION, doorActionAction,
                                                                  self.__close_door_action_cb, auto_start=False)
            self.close_door_action.register_preempt_callback(self.__close_door_interrupt_cb)
            self.close_door_action.start()

            self.door_calibration_action = actionlib.SimpleActionServer(DOOR_CALIBRATION_ACTION,
                                                                        doorCalibrationActionAction,
                                                                        self.__calibration_action_cb,
                                                                        auto_start=False)
            self.door_calibration_action.register_preempt_callback(self.__door_calibration_interrupt_cb)
            self.door_calibration_action.start()

            self.torque_allowed_service = rospy.Service(TORQUE_ALLOWED_SERVICE, doorSrvTorqueAllowed,
                                                        self.__torque_allowed_service_cb)

        if DOOR_TYPE == 'manual':
            pass

    # Callbacks are defined here
    @_service_function_decorator
    def __torque_allowed_service_cb(self, command: doorSrvTorqueAllowedRequest):
        self.door.set_torque(command.torque)

    @_service_function_decorator
    def __calibration_action_cb(self):
        result = doorCalibrationActionResult()
        feedback = doorCalibrationActionFeedback()
        result.details = self.door.calibrate_door()
        self.door_calibration_action.set_succeeded(result)

    @_service_function_decorator
    def __open_door_action_cb(self, command: doorActionGoal):
        # TODO: Split the driver sequence in order to have a feedback
        result = doorActionResult()
        feedback = doorActionFeedback()
        if not command.command:
            self.door.stop_door_motion()
            self.__open_door_interrupt_cb()
        else:
            result.details = self.door.door_action(command.command, self.__convert_string_to_int_list(command.door_list))
            if self.open_door_action.is_active():
                self.open_door_action.set_succeeded(result)

    @_service_function_decorator
    def __close_door_action_cb(self, command: doorActionGoal):
        # TODO: Split the driver sequence in order to have a feedback
        result = doorActionResult()
        feedback = doorActionFeedback()
        if not command.command:
            self.door.stop_door_motion()
            self.__close_door_interrupt_cb()
        else:
            result.details = self.door.door_action(command.command,
                                                   self.__convert_string_to_int_list(command.door_list))
            if self.close_door_action.is_active():
                self.close_door_action.set_succeeded(result)

    # @_service_function_decorator
    # def __open_door_action_cb(self, command: doorActionGoal):
    #     # TODO: Split the driver sequence in order to have a feedback
    #     result = doorActionResult()
    #     feedback = doorActionFeedback()
    #     result.details = self.door.door_action(command.command, self.__convert_string_to_int_list(command.door_list))
    #     if self.open_door_action.is_active():
    #         self.open_door_action.set_succeeded(result)
    #
    # @_service_function_decorator
    # def __close_door_action_cb(self, command: doorActionGoal):
    #     # TODO: Split the driver sequence in order to have a feedback
    #     result = doorActionResult()
    #     feedback = doorActionFeedback()
    #     result.details = self.door.door_action(command.command, self.__convert_string_to_int_list(command.door_list))
    #     if self.close_door_action.is_active():
    #         self.close_door_action.set_succeeded(result)

    _service_function_decorator = staticmethod(_service_function_decorator)

    def __open_door_interrupt_cb(self):
        result = doorActionResult()
        # result.details = self.door.stop_door_motion()
        result.details = 'Open door action interrupted'
        self.door.set_state_to_preempt()
        self.open_door_action.set_preempted(result)

    def __close_door_interrupt_cb(self):
        result = doorActionResult()
        # result.details = self.door.stop_door_motion()
        result.details = 'Close door action interrupted'
        self.door.set_state_to_preempt()
        self.close_door_action.set_preempted(result)

    def __door_calibration_interrupt_cb(self):
        result = doorCalibrationActionResult()
        result.details = self.door.stop_door_motion()
        self.door_calibration_action.set_preempted(result)

    @staticmethod
    def __initialize_door():
        try:
            door = None
            door_type = rospy.get_param("door_type", 'motorized')
            door_num = rospy.get_param("door_num", 3)
            door_port = rospy.get_param("door_port", '/dev/serial/by-id/usb-FTDI_FT230X_Basic_UART_D308B55J-if00-port0')

            if door_type == 'motorized':
                door = SerialDoors(door_num, serial_port=door_port)
            elif door_type == 'manual':
                rospy.loginfo(f'----- [DOOR] ***** Doors type is : {door_type} ***** -----')
            else:
                raise ValueError("No driver for this Door's type")
            return door
        except Exception:
            rospy.logerr(f'----- [DOOR] ***** {traceback.format_exc()} ***** -----')

    @staticmethod
    def __convert_string_to_int_list(data):
        data = data.split()
        for num in range(0, len(data)):
            data[num] = int(data[num])
        return data

    def node_shutdown(self):
        rospy.loginfo("----- [DOOR] ***** Node shutting down ***** -----")
        self.door_logs.unregister()
        # Do not forget to close services
        # self.door_service.shutdown()


if __name__ == "__main__":
    try:
        rospy.init_node(os.path.splitext(os.path.basename(__file__))[0])
        node = DoorServices()
        rospy.on_shutdown(node.node_shutdown)
        node.begin()
        rospy.spin()
        # signal.signal(signal.SIGINT, node.signal_handler)
    except Exception:
        #print(traceback.format_exc())
        rospy.logerr(traceback.format_exc())

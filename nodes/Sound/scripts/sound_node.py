#!/usr/bin/env python

import os

import actionlib
import rospy
import traceback
from std_msgs.msg import String
from sound_drivers.sound_omron import OmronSound
from sound_package.msg import soundActionAction, soundActionGoal, soundActionResult
"""

"""

# sound config
SOUND_HARDWARE = rospy.get_param("sound_hardware", "omron")
SOUND_PORT = rospy.get_param("port", 0)


# ros topic names
PLAY_PAUSE_SUB = rospy.get_param('play_pause', 'play_pause_sound')
SOUND_ACTION = rospy.get_param('sound_action', 'sound_action')


class sound_node:
    def __init__(self):
        self.pause_play_sub = rospy.Subscriber(PLAY_PAUSE_SUB, String, self.__pause_play_cb)
        self.sound_action = actionlib.SimpleActionServer(SOUND_ACTION, soundActionAction,
                                                         self.__sound_action_cb, auto_start=False)
        self.sound_action.register_preempt_callback(self.__sound_action_interrupt_cb)
        self.current_sound = {'type':'', 'info': '', 'time': 0}
        self.__select_sound_hardware()

    def begin(self):
        try:
            if self.sound_hardware.connect():
                self.sound_action.start()
            else:
                rospy.logerr("----- [SOUND] ***** Sound hardware could not connect ***** ------")

        except Exception:
            rospy.logerr(f'----- [SOUND] ***** Initialisation failed: {traceback.format_exc()} ***** -----')
            rospy.signal_shutdown("Initialisation Failed")

    def node_shutdown(self):
        rospy.loginfo("----- [SOUND] ***** Node is shutting down ***** -----")

    def __sound_action_cb(self, sound_goal: soundActionGoal):
        rospy.loginfo(f"------ [START]+[SOUND] ***** Sound action: {self.current_sound} begin ***** ----- ")
        self.current_sound['type'] = sound_goal.type
        self.current_sound['info'] = sound_goal.info
        self.current_sound['time'] = self.__compute_time_voice_text(self.current_sound['info'])
        self.sound_hardware.play(self.current_sound)
        result = soundActionResult()
        result.details = 'success'
        self.sound_action.set_succeeded(result)
        rospy.loginfo(f"------[END]+[SOUND] ***** Sound action: {self.current_sound} was successful ***** ----- ")

    def __sound_action_interrupt_cb(self):
        rospy.loginfo(f"------ [END]+[SOUND] ***** Sound action: {self.current_sound} was interrupted ***** ----- ")
        self.sound_hardware.stop()
        result = soundActionResult()
        result.details = 'Sound interrupt'
        self.sound_action.set_preempted(result)

    def __pause_play_cb(self, command: String):
        rospy.loginfo(f"------ [SOUND] ***** Sound action: {command.data} is still not implemented ***** ----- ")
        pass

    def __select_sound_hardware(self):
        if SOUND_HARDWARE == 'omron':
            self.sound_hardware = OmronSound(SOUND_PORT)
        elif SOUND_HARDWARE == 'other':
            pass

    @staticmethod
    def __compute_time_voice_text(text):
        nb_of_words = 0
        for letter in text:
            if letter == " ":
                nb_of_words += 1
        # The average speaking speed is 3 words per second
        time_in_sec = nb_of_words / 3.0
        return int(time_in_sec)


if __name__ == "__main__":
    try:
        rospy.init_node(os.path.splitext(os.path.basename(__file__))[0])
        node = sound_node()
        rospy.on_shutdown(node.node_shutdown)
        node.begin()
        rospy.spin()
        # signal.signal(signal.SIGINT, node.signal_handler)
    except Exception:
        print(traceback.format_exc())
        rospy.logerr(traceback.format_exc())

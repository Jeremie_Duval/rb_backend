#!/usr/bin/env python

import os
import rospy
import functools
import traceback
from std_msgs.msg import String
from led_node.srv import ledSrvColor, ledSrvColorRequest
from led_node.srv import ledSrvRGBColor, ledSrvRGBColorRequest
from led_node.srv import ledSrvBlinking, ledSrvBlinkingRequest
from led_node.srv import ledSrvPeriod, ledSrvPeriodRequest
from led_node.srv import ledSrvMultiple, ledSrvMultipleRequest
from led_drivers.led_7_colors import Led7Colors, LedClass
from led_drivers.led_rgb_pwm import LedRGBPWM, LedClass

"""

"""
# ros topic/service names
LED_TYPE = rospy.get_param('led_type', 'led_7_colors')
LED_COLOR = rospy.get_param('led_colors', 'led_set_color')
LED_RGB_COLOR = rospy.get_param('led_rgb_color', 'led_set_rgb_color')
LED_BLINKING = rospy.get_param('led_blinking', 'led_set_blinking')
LED_PERIOD_MS = rospy.get_param('led_period_ms', 'led_set_period_ms')
LED_MULTIPLE = rospy.get_param('led_multiple', 'led_set_multiple')
LED_PUBLISHER = rospy.get_param('led_logs', 'led_logs')


class LedServices:
    def _service_function_decorator(function):
        @functools.wraps(function)
        def wrapper(self, *args):
            self.led_logs.publish(f"{args}")
            answer = [False, ""]
            try:
                function(self, *args)
                answer[0] = True
            except ValueError as val_err:
                rospy.logerr(f'----- [LED] ***** {str(val_err)} ***** -----')
                answer[1] = str(val_err)
            finally:
                self.led_logs.publish(f" Success: {answer[0]}, details:{answer[1]}")
                return answer

        return wrapper

    def __init__(self):
        self.led: LedClass = None
        self.led = self.__initialize_led()

        self.led_color: rospy.Service = None
        self.led_rgb_color: rospy.Service = None
        self.led_blinking: rospy.Service = None
        self.led_period: rospy.Service = None
        self.led_multiple: rospy.Service = None
        self.led_logs: rospy.Publisher = None

    def begin(self):
        try:
            if self.led.connect():
                # Initialisation with Callback
                self.__start_services()
                rospy.loginfo(f'----- [LED] ***** LED connected! ***** -----')
            else:
                rospy.logerr("----- [LED] ***** Led did not connect ***** -----")

        except Exception:
            rospy.logerr(f'----- [LED] ***** Initialisation failed: {traceback.format_exc()} ***** -----')
            rospy.signal_shutdown("Initialisation Failed")

    def __start_services(self):

        self.led_color = rospy.Service(LED_COLOR, ledSrvColor, self.__color_cb)
        self.led_logs = rospy.Publisher(LED_PUBLISHER, String, queue_size=5)

        if LED_TYPE == 'led_7_colors':
            self.led_blinking = rospy.Service(LED_BLINKING, ledSrvBlinking, self.__blinking_cb)
            self.led_period = rospy.Service(LED_PERIOD_MS, ledSrvPeriod, self.__period_cb)
            self.led_multiple = rospy.Service(LED_MULTIPLE, ledSrvMultiple, self.__multiple_cb)

        if LED_TYPE == 'led_rgb_pwm':
            self.led_rgb_color = rospy.Service(LED_RGB_COLOR, ledSrvRGBColor, self.__rgb_color_cb)

    def node_shutdown(self):
        rospy.loginfo("----- [LED] ***** Node shutting down ***** -----")
        self.led_color.shutdown()

        if LED_TYPE == 'led_7_colors':
            self.led_blinking.shutdown()
            self.led_period.shutdown()
            self.led_multiple.shutdown()

        if LED_TYPE == 'led_rgb_pwm':
            self.led_rgb_color.shutdown()

        self.led_logs.unregister()

    @_service_function_decorator
    def __color_cb(self, command: ledSrvColorRequest):
        self.led.set_color(command.color)

    @_service_function_decorator
    def __rgb_color_cb(self, command: ledSrvRGBColorRequest):
        self.led.set_rgb_color(command.r, command.g, command.b)

    @_service_function_decorator
    def __blinking_cb(self, command: ledSrvBlinkingRequest):
        self.led.set_blinking_state(command.blinking)

    @_service_function_decorator
    def __period_cb(self, command: ledSrvPeriodRequest):
        self.led.set_period(command.period_ms)

    @_service_function_decorator
    def __multiple_cb(self, command: ledSrvMultipleRequest):
        self.led.set_multiple_value(command.color, command.blinking, command.period_ms)

    _service_function_decorator = staticmethod(_service_function_decorator)

    @staticmethod
    def __initialize_led():
        try:
            led = None
            led_type = rospy.get_param("led_type", 'led_7_colors')
            color = rospy.get_param('led_initial_color', 'off')
            blink = rospy.get_param('led_initial_blink', False)
            period_ms = rospy.get_param('led_initial_period_ms', 500)
            baud_rate = rospy.get_param('led_baud_rate', 9600)

            if led_type == 'led_7_colors':
                port = rospy.get_param("led_7_colors_port",
                                       '/dev/serial/by-id/usb-FTDI_FT230X_Basic_UART_DN05XJJL-if00-port0')
                led = Led7Colors(baud_rate, port, color, blink, period_ms)
            elif led_type == 'led_rgb_pwm':
                port = rospy.get_param("led_rgb_pwm_port",
                                       '/dev/serial/by-id/usb-FTDI_FT230X_Basic_UART_DN05XJJM-if00-port0')
                led = LedRGBPWM(baud_rate, port, color, blink, period_ms)
            else:
                raise ValueError("No driver for this Led's type")
            return led
        except Exception:
            rospy.logerr(f'----- [LED] ***** {traceback.format_exc()} ***** -----')


def dummy_fct():
    pass


if __name__ == "__main__":
    try:
        rospy.init_node(os.path.splitext(os.path.basename(__file__))[0])
        node = LedServices()
        rospy.on_shutdown(node.node_shutdown)
        node.begin()
        rospy.spin()
        # signal.signal(signal.SIGINT, node.signal_handler)
    except Exception:
        rospy.logerr(traceback.format_exc())

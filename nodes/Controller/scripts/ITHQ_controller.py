#!/usr/bin/env python

import os
import csv
import glob
import time
import rospy
import datetime
import actionlib
import threading
import traceback
from std_msgs.msg import String, Bool, Empty
from comm_protocols_drivers.telnet_client import TelnetOmron
from omron_base_nodes.srv import arclSrv, arclSrvRequest, arclSrvResponse
from ui_user_nodes.msg import pantheraGoalsList, pantheraGoals, pantheraTravellingMsg
from omron_base_nodes.msg import gotoActionAction, gotoActionGoal, gotoActionResult, gotoActionFeedback
from ui_user_nodes.msg import timerActionAction, timerActionGoal, timerActionResult, timerActionFeedback

TIMEOUT=10

TEXTE_DEBUT = ("Bonjour! Bienvenue à l’ I Thé ash cul, votre nouvelle école!", 5)
DIRECTION_ACCUEIL=("Suivez-moi, je vais vous escorter et vous indiquer le chemin pour vous rendre à la salle où se tiendra votre accueil.", 8)
ARRET_ACCUEIL=("Nous nous trouvons présentement dans le all de l’hôtel... Saviez-vous que notre hôtel abrite 42 chambres et qu’elles sont un lieu d’apprentissage?... L’hôtel est présentement en rénovations majeures. Vous recevrez éventuellement des nouvelles concernant sa Ré Ouverture! À votre droite se trouve le comptoir de réception de l’hôtel, là où nous accueillons les clients.", 23 )
RETOURNE_ACCUEIL=("Derrière moi, l’escalier, B..1. Vous devez emprunter cet escalier pour accéder aux vestiaires des femmes.... Veuillez me suivre....",9)
MARCHE_VERS_ESCALIER= ("... ... Nous entrons maintenant dans la partie école. À votre droite, le bureau de la sécurité. Si jamais vous vous blessez ou si vous perdez un objet personnel, présentez-vous ici et un agent de sécurité pourra vous venir en aide.... Continuons", 16)
ARRET_ESCALIER=("Au bout du couloir, vous pourrez y découvrir votre Café étudiant, un endroit où vous pouvez aller « tchillé» ou manger... Quant aux vestiaires des hommes, ils sont complètement à gauche, après le Café étudiant....  En terminant, laissez-moi vous parler d’un simple principe de l’accueil que nous adoptons ici à l’école...  lorsqu’on croise quelqu’un à 3 mètres de nous, on fait un signe de la tête!... Et à 1 mètre, on dit bonjour. Une petite règle d’or qu’on vous invite à pratiquer!",33)
RETOURNE_VERS_ESCALIER=(" C’est ici que se termine cette petite visite. Si cela peut vous être utile dans votre organisation scolaire, je vous invite à prendre un agenda. Il est maintenant temps de prendre les escaliers roulants jusqu’au 2e étage. Je vous souhaite une belle rencontre et surtout, une belle session!",35)
FIN=("Si vous voulez rire un peu, n'hésitez pas à péser sur le champignon de mon écran ...hihihihi!", 7)

G_DEBUT="entrée"
G_ACCUEIL="hall"
G_ACCUEIL_RETURN="vestiaire"
G_TEXT="texte"
G_ESCALIER="café"
G_ESCALIER_RETURN="escaliers"
G_360_TURN="fin_sequence"

BLAGUES = {0:("C'est l'histoire d'un aveugle qui rentre dans un bar....Et dans une table, et dans une chaise, et dans un mur...", 5),
           1:("C'est quoi une chauve souris avec une perruque ? ... Une souris.", 5),
           2:("Qu'est ce qui n'est pas un steak ? ...Une pastèque.", 5),
           3:("Une fesse gauche rencontre une fesse droite : .... « Tu ne trouves pas que ça pue dans le couloir ? »", 5),
           4:("Comment s'appelle les fesses d'un Schtroumpf ?.... Un blue-ré.", 5),
           5:("C'est l'histoire de 2 patates qui traversent la route... L’une d’elle se fait écraser. L’autre dit : « Oh purée ! »", 5),
           6:("Pourquoi faut-il enlever ses lunettes avant un alcootest ? ... Ca fait 2 verres en moins.", 5),
           7:("Comment appelle-t-on un chien qui n'a pas de pattes ? ... On ne l’appelle pas, on va le chercher ...", 5),
           8:("Que fait un crocodile quand il rencontre une superbe femelle ?...Il Lacoste.", 5),
           9:("Pourquoi les vaches ferment_elles les yeux pendant la traite de lait ?... Pour faire du lait concentré.", 5),
           10:("Quel est le point commun entre les mathématiques et le sexe ?... Plus il y a d’inconnues, plus c’est chaud.", 5),
           11:("2 vaches discutent... :  Ça te fait pas peur toi ces histoires de « vache folle » ...? Ben j’m’en fous j’suis un lapin !", 5),
           12:("Tu connais l'histoire du lit superposé ? ...C’est une histoire à dormir debout.", 5)}

TASK1={'navigation':G_DEBUT, 'speaking': None}
TASK2={'navigation':None, 'speaking': TEXTE_DEBUT}
TASK3={'navigation':G_ACCUEIL, 'speaking': DIRECTION_ACCUEIL}
TASK4={'navigation':None, 'speaking': ARRET_ACCUEIL}
TASK5={'navigation':G_ACCUEIL_RETURN, 'speaking': None}
TASK6={'navigation':None, 'speaking': RETOURNE_ACCUEIL}
TASK7={'navigation':G_TEXT, 'speaking': None}
TASK8={'navigation':G_ESCALIER, 'speaking': MARCHE_VERS_ESCALIER}
TASK9={'navigation':None, 'speaking': ARRET_ESCALIER}
TASK10={'navigation':G_ESCALIER_RETURN, 'speaking': None}
TASK11={'navigation':None, 'speaking': RETOURNE_VERS_ESCALIER}
TASK12={'navigation':None, 'speaking': FIN}


class ITHQController:
    def __init__(self):
        self.task_list = [TASK1,TASK2,TASK3,TASK4,TASK5,TASK6,TASK7,TASK8,TASK9,TASK10,TASK11]
        self.estop_sub = rospy.Subscriber('estop', Bool, self.estop_cb, queue_size=10)
        self.panth_restart_sub = rospy.Subscriber('panth_delivery_goal', String, self.__restart_cb, queue_size=5)
        self.action_go_to_client = actionlib.SimpleActionClient('arcl_goto_action', gotoActionAction)
        self.speak_client = rospy.ServiceProxy('arcl_services', arclSrv)
        self.panth_list_goal_pub = rospy.Publisher("panth_list_goals", pantheraGoalsList, queue_size=5)

        self.ui_command_sub = rospy.Subscriber('ui_general_cmd', String,self.ui_command_cb, queue_size=10)
        self.ui_timer_client = actionlib.SimpleActionClient('panthera_timer', timerActionAction)
        self.ui_text_pub = rospy.Publisher('panth_text', pantheraTravellingMsg, queue_size=10)

        self.task_loop_thread = threading.Thread(target=self.task_loop)
        self.speak_loop_thread = threading.Thread(target=self.speaking_service_with_time)
        self.speaking_action = None
        self.goto_goal = None
        self.goto_action = gotoActionGoal()
        self.goto_action.command = 'goto'
        self.speaking_lock = threading.Lock()

        self.index = 0
        self.estop_pressed=False
        self.joke_key = 0
        self.pause = False
        self.goto_success = False
        self.nav_error = False

        self.restart=False
        self.speak_is_usable=False

    def begin(self):
        timeout = rospy.Duration(10)
        if self.ui_timer_client.wait_for_server():
            print("ui_timer_ok")

        if self.action_go_to_client.wait_for_server():
            print("goto action ok")

        # if rospy.wait_for_service("arcl_services"):
        #     print("speak_client ok")
        #     rospy.loginfo("All services are connected")

        self.task_loop_thread.start()
        self.speak_loop_thread.start()
        self.speak_is_usable = True

        time.sleep(1)
        panthera_restart = pantheraGoals()
        panthera_restart.goal_id = 'restart'
        panthera_restart.goal_name = 'restart'
        panthera_restart_list = pantheraGoalsList()
        panthera_restart_list.list_of_goals = [panthera_restart]

        self.panth_list_goal_pub.publish(panthera_restart_list)

        # else:
        #     rospy.logerr("A service did not connect")
        #     rospy.signal_shutdown("Please check for service connection")

    # def task_loop(self):
    #     next_task = True
    #     wait_for_goto_success = False
    #     publish_once = True
    #     text_ui = pantheraTravellingMsg()
    #     text_ui.text1 = "Bienvenue à l'ITHQ"
    #     text_ui.color1 = "black"
    #     text_ui.text2 = "Pour commencer la présentation des lieux, faire PLAY"
    #     text_ui.color2 = "green"
    #     self.ui_text_pub.publish(text_ui)
    #     while not rospy.is_shutdown():
    #         if (next_task or self.restart) and not self.pause:
    #             self.speaking_lock.acquire()
    #             self.goto_goal = self.task_list[self.index].get('navigation', None)
    #             self.speaking_action = self.task_list[self.index].get('speaking', None)
    #             self.speaking_lock.release()
    #             if self.goto_goal is not None:
    #                 text_ui.text2 = f"Prochaine destination: {self.__get_displayed_name()}"
    #                 text_ui.color2 = "gray"
    #                 self.ui_text_pub.publish(text_ui)
    #                 self.goto_action.goal = self.goto_goal
    #                 self.action_go_to_client.send_goal(self.goto_action)
    #             self.restart = False
    #             wait_for_goto_success = False
    #             next_task = False
    #
    #         if self.goto_goal is not None:
    #             self.action_go_to_client.wait_for_result()
    #             if self.action_go_to_client.get_state() == 3:
    #                 next_task = True
    #                 wait_for_goto_success = False
    #             else:
    #                 wait_for_goto_success = True
    #
    #         if self.speaking_action is not None:
    #             self.speaking_lock.acquire()
    #             self.speaking_lock.release()
    #             next_task = True
    #
    #         if wait_for_goto_success:
    #             next_task = False
    #
    #         if next_task:
    #             self.index += 1
    #             self.index = self.index % len(self.task_list)
    #             if self.index == 1:
    #                 next_task = False
    #                 self.__pause_sequence(True)
    #
    #         time.sleep(0.5)

    def task_loop(self):
        next_task = True
        wait_for_goto_success = False
        text_beginning = pantheraTravellingMsg()
        text_beginning.text1 = "Bienvenue à l'ITHQ"
        text_beginning.color1 = "black"
        self.ui_text_pub.publish(text_beginning)
        while not rospy.is_shutdown():
            if not self.pause:
                self.__run_task()
                self.__wait_for_result()
                self.__update_index()
                self.__look_for_specific_behavior()

            time.sleep(0.5)

    def __run_task(self):
        rospy.logerr("In run task")
        self.speaking_lock.acquire()
        self.goto_goal = self.task_list[self.index].get('navigation', None)
        self.speaking_action = self.task_list[self.index].get('speaking', None)
        self.speaking_lock.release()
        if self.goto_goal is not None:  # This looks if there is a navigation goal
            ui_text = pantheraTravellingMsg()
            ui_text.text2 = f"Prochaine destination: {self.__get_displayed_name()}"
            ui_text.color2 = "gray"
            self.ui_text_pub.publish(ui_text)
            self.goto_action.goal = self.goto_goal
            self.action_go_to_client.send_goal(self.goto_action)

    def __wait_for_result(self):
        if self.goto_goal is not None:  # GESTION DU GOAL DE NAVIGATION
            self.action_go_to_client.wait_for_result()
            rospy.logwarn(self.action_go_to_client.get_state())
            if self.action_go_to_client.get_state() == 3:  # SUCCESS
                # Pourrait effectuer un suivi avec les actions pour avoir le résultat dans une clé précise
                self.goto_success = True
            elif self.action_go_to_client.get_state() == 4:  # ERROR
                rospy.loginfo("Could not reach goal, robot will be paused to look why")
                self.nav_error = True
                ui_text = pantheraTravellingMsg()
                ui_text.text1 = f"Erreur: L'objectif est inatteignable"
                ui_text.color1 = "red"
                self.ui_text_pub.publish(ui_text)
                self.__pause_sequence()
            elif self.action_go_to_client.get_state() == 6:  # INTERROMPU
                rospy.loginfo("Could not reach goal, robot interrupted")

        if self.speaking_action is not None:  # Attendre que la parole soit terminé
            self.speaking_lock.acquire()
            self.speaking_lock.release()

    def __update_index(self):
        if self.goto_goal is None or self.goto_success:
            self.index += 1
            self.index = self.index % len(self.task_list)
            self.goto_success = False

    def __look_for_specific_behavior(self):
        if self.index == 1:
            self.__pause_sequence(True)


    def estop_cb(self, pressed: Bool):
        pressed = pressed.data
        if pressed:
            self.estop_pressed = True
            self.index = 0
        else:
            self.estop_pressed = False

    def ui_command_cb(self, command: String):
        command = command.data
        rospy.loginfo(command)
        if command == 'dock':
            rospy.loginfo("Event dock for controller")
            if not self.estop_pressed:
                ui_text = pantheraTravellingMsg()
                ui_text.text2 = f"Prochaine destination: Station de recharge"
                self.ui_text_pub.publish(ui_text)
                action_goal = gotoActionGoal()
                action_goal.command = command
                self.pause=True
                self.nav_error=False
                self.action_go_to_client.send_goal(action_goal)
                self.goto_success = False
                self.index = 0

        if command == 'play':
            rospy.loginfo("Event play for controller")
            if not self.estop_pressed:
                self.pause = False
                ui_text = pantheraTravellingMsg()
                if self.nav_error:
                    ui_text.text1 = "Bienvenue à l'ITHQ"
                    ui_text.color1 = "black"
                    self.nav_error = False
                if self.goto_goal is not None:
                    ui_text.text2 = f"Prochaine destination: {self.__get_displayed_name()}"
                else:
                    ui_text.clean2 = True
                try:
                    self.ui_text_pub.publish(ui_text)
                except Exception:
                    print(traceback.format_exc())

        if command == 'pause':
            if not self.estop_pressed:
                rospy.loginfo("Event pause for controller")
                self.__pause_sequence()

        if command == 'joke':
            if self.speak_is_usable:
                self.speaking_lock.acquire()
                self.speaking_action = BLAGUES.get(self.joke_key)
                self.speaking_lock.release()
                self.joke_key += 1
                self.joke_key = self.joke_key % len(BLAGUES)
                # self.speak_loop_thread.join()
                self.speak_loop_thread.start()

    def __restart_cb(self, restart_query):
        if not self.estop_pressed:
            rospy.loginfo("Event restart for controller")
            ui_text = pantheraTravellingMsg()
            ui_text.text2 = f"Prochaine destination: Point de départ"
            self.ui_text_pub.publish(ui_text)
            self.index = 0
            self.nav_error = False
            self.goto_success = False
            action = gotoActionGoal()
            action.command = "goto"
            action.goal = "entrée"
            self.action_go_to_client.send_goal(action)

    # def ui_command_cb(self, command: String):
    #     command = command.data
    #     rospy.loginfo(command)
    #     rospy.loginfo(command)
    #     if command == 'dock':
    #         if not self.estop_pressed:
    #             action_goal = gotoActionGoal()
    #             action_goal.command = command
    #             self.action_go_to_client.send_goal(action_goal)
    #
    #     if command == 'play':
    #         if not self.estop_pressed:
    #             if self.action_go_to_client.get_state() <= 1: # 1=ACTIVE
    #                 pass
    #             else:
    #                 self.action_go_to_client.send_goal(self.goto_action)
    #                 ui_text = pantheraTravellingMsg()
    #                 if self.goto_goal is not None:
    #                     ui_text.text2 = f"Prochaine destination: {self.__get_displayed_name()}"
    #                 else:
    #                     ui_text.clean2 = True
    #                 try:
    #                     self.ui_text_pub.publish(ui_text)
    #                 except Exception:
    #                     print(traceback.format_exc())
    #         self.pause = False
    #
    #     if command == 'pause':
    #         self.__pause_sequence()
    #
    #     if command == 'joke':
    #         if self.speak_is_usable:
    #             self.speaking_lock.acquire()
    #             self.speaking_action = BLAGUES.get(self.joke_key)
    #             self.speaking_lock.release()
    #             self.joke_key += 1
    #             self.joke_key = self.joke_key % len(BLAGUES)
    #             # self.speak_loop_thread.join()
    #             self.speak_loop_thread.start()
    #
    # def __restart_cb(self, restart_query):
    #     # print("wouhou!")
    #     self.action_go_to_client.cancel_all_goals()
    #     self.index = 0
    #     self.pause = False
    #     self.restart = True

    def speaking_service_with_time(self):
        while not rospy.is_shutdown():
            if self.speaking_action is not None:
                self.speaking_lock.acquire()
                self.speak_is_usable = False
                arcl_service_goal = arclSrvRequest()
                arcl_service_goal.command = f"say {self.speaking_action[0]}"
                panth_timer = timerActionGoal()
                panth_timer.time = self.speaking_action[1]
                panth_timer.is_stoppable = False
                self.speak_client.call(arcl_service_goal)
                # time.sleep(5)
                self.ui_timer_client.send_goal(panth_timer)
                self.ui_timer_client.wait_for_result()
                self.speaking_action = None
                self.speaking_lock.release()
                self.speak_is_usable = True
            time.sleep(0.5)

    def __get_displayed_name(self):
        # name = f"{}self.goto_goal.split("_")[0]
        return self.goto_goal

    def __pause_sequence(self, begin = False):
        if self.action_go_to_client.get_state() == 1:
            self.action_go_to_client.cancel_goal()
        self.pause = True
        ui_text = pantheraTravellingMsg()
        if begin:
            ui_text.text2 = "Pour commencer la présentation des lieux, faire PLAY"
            ui_text.color2 = "green"
        else:
            ui_text.text2 = "PAUSE"
            ui_text.color2 = "gray"
        self.ui_text_pub.publish(ui_text)


if __name__ == "__main__":
    try:
        rospy.init_node(os.path.splitext(os.path.basename(__file__))[0])
        node = ITHQController()
        # rospy.on_shutdown(node.node_shutdown)
        node.begin()
        rospy.spin()
        # signal.signal(signal.SIGINT, node.signal_handler)
    except Exception:
        rospy.logerr(traceback.format_exc())

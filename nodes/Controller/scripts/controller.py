#!/usr/bin/env python

import os
import csv
import glob
import time
from rosnode import rosnode_ping
import rospy
import datetime
import actionlib
import threading
import traceback
from std_msgs.msg import String, Bool, Empty
from comm_protocols_drivers.sms_client import SmsClient
from comm_protocols_drivers.phone_call_client import PhoneCallClient
from omron_base_nodes.srv import arclSrv
from omron_base_nodes.msg import gotoActionAction, gotoActionGoal
from ui_user_nodes.msg import pantheraGoalsList,  travellingMsg
from ui_user_nodes.msg import timerActionAction, timerActionGoal
from sound_package.msg import soundActionAction, soundActionGoal
from led_node.srv import ledSrvBlinking, ledSrvColor, ledSrvRGBColor, ledSrvMultiple, ledSrvPeriod
from robot_doors_node.msg import doorActionAction, doorActionGoal
from robot_doors_node.msg import doorCalibrationActionAction, doorCalibrationActionGoal
from robot_doors_node.srv import doorSrvTorqueAllowed

LOCALIZATION_POINT = '0 0 0'
PASSWORD_FOR_LOCALIZATION = '1234'
CELL_JO = '15146879950'
CELL_JER = '14505024001'

SMS_TEXT = {
    "route1": 'Vous êtes maintenant arrivés à la destination de la route 1.'
}

BLAGUES = {
    0: "C'est l'histoire d'un aveugle qui rentre dans un bar....Et dans une table, et dans une chaise, et dans un mur...",
    1: "C'est quoi une chauve souris avec une perruque ? ... Une souris.",
    2: "Qu'est ce qui n'est pas un steak ? ...Une pastèque.",
    3: "Une fesse gauche rencontre une fesse droite : .... « Tu ne trouves pas que ça pue dans le couloir ? »",
    4: "Comment s'appelle les fesses d'un Schtroumpf ?.... Un blue-ré.",
    5: "C'est l'histoire de 2 patates qui traversent la route... L’une d’elle se fait écraser. L’autre dit : « Oh purée ! »",
    6: "Pourquoi faut-il enlever ses lunettes avant un alcootest ? ... Ca fait 2 verres en moins.",
    7: "Comment appelle-t-on un chien qui n'a pas de pattes ? ... On ne l’appelle pas, on va le chercher ...",
    8: "Que fait un crocodile quand il rencontre une superbe femelle ?...Il Lacoste.",
    9: "Pourquoi les vaches ferment_elles les yeux pendant la traite de lait ?... Pour faire du lait concentré.",
    10: "Quel est le point commun entre les mathématiques et le sexe ?... Plus il y a d’inconnues, plus c’est chaud.",
    11: "2 vaches discutent... :  Ça te fait pas peur toi ces histoires de « vache folle » ...? Ben j’m’en fous j’suis un lapin !",
    12: "Tu connais l'histoire du lit superposé ? ...C’est une histoire à dormir debout.",
    13: "C’est l’histoire d’un poil...  avant il était bien, et maintenant il est pubien",
    14: "Est-ce que vous savez qu’elle est le fruit préféré de Donald Trump? ... C’est les mûrs",
    15: "C’est un pain au chocolat qui rencontre un croissant et qui lui dit : , "
        "Eh, pourquoi t’es en forme de lune toi ? ... Le croissant répond : ,"
        " Oh, j’t’en pose des questions, moi ? ... Est-ce que j’te demande pourquoi t’as une merde au cul ?",
    16: "Toto a eu 20/20 en rédaction et sa maitresse lui dit: , Tu peux me l’avouer, ta mère ta aidée. ... "
        "Non, elle ne m’a pas aidée, elle la faite toute seule.",
    17: "A la piscine, un nageur se fait enguirlander parce qu’il a fait pipi dans l’eau. "
        " Mais enfin, proteste-t-il, vous exagérez, je ne suis pas le seul à faire ça ! "
        "Si, monsieur, du haut du plongeoir, vous êtes le seul !",
    18: "Que dit un rouleau de papier de toilette déguisé en Dark-Vador? ... J’essuie ton père!!!",
    19: "Quel est le crustacé le plus léger de la mer ? ...  La pas lourde"}

# Task's structure :
# {'navigation': {'type': 'goal', 'id': (string)}}
#                {'type': 'point', 'id': goal_name (string), 'coord': (string)}
#
# {'sound': {'type': 'speak', 'text': (string)}}
#
# {'door' : {'type': 'open/close/calibration', 'id': (string)}}
#
# {'led': {'type': 'color', 'color': (string)}}
#         {'type': 'rgb_color', 'r': (int), 'g': (int), 'b': (int)}
#         {'type': 'blinking', 'state': (bool)}
#         {'type': 'period', 'period': (int - ms)}
#         {'type': 'multiple', 'color': (string), 'state': (bool), 'period': (int - ms)}
#
# {'standby': {'type': 'wait', 'time': (int), 'stoppable': (bool -- optional)}}
#
# {'message': {'type': 'sms/voice_call', 'phone_num': (string), 'text': (string)}}


TASK_LIST = {
    "goal1": {'navigation': {'type': 'goal', 'id': 'Goal1'}},
    "goal2": {'navigation': {'type': 'goal', 'id': 'Goal2'}},
    "goal3": {'navigation': {'type': 'goal', 'id': 'Goal3'}},
    "goal4": {'navigation': {'type': 'goal', 'id': 'Goal4'}},
    "goal5": {'navigation': {'type': 'goal', 'id': 'Goal5'}},
    "goal6": {'navigation': {'type': 'goal', 'id': 'Goal6'}},

    "open_doors": {'door': {'type': 'open', 'id': "0 1 2"}},
    "close_doors": {'door': {'type': 'close', 'id': "0 1 2"}},
    "open_door_1": {'door': {'type': 'open', 'id': "0"}},
    "close_door_1": {'door': {'type': 'close', 'id': "0"}},
    "open_door_2": {'door': {'type': 'open', 'id': "1"}},
    "close_door_2": {'door': {'type': 'close', 'id': "1"}},
    "open_door_3": {'door': {'type': 'open', 'id': "2"}},
    "close_door_3": {'door': {'type': 'close', 'id': "2"}},

    "cyan_light": {'led': {'type': 'color', 'color': "cyan"}},
    "green_light": {'led': {'type': 'color', 'color': "green"}},
    "blue_light": {'led': {'type': 'color', 'color': "blue"}},
    "yellow_light": {'led': {'type': 'color', 'color': "yellow"}},
    "orange_light": {'led': {'type': 'color', 'color': "orange"}},
    "violet_light": {'led': {'type': 'color', 'color': "violet"}},
    "white_light": {'led': {'type': 'color', 'color': "white"}},

    "wait_3sec": {'standby': {'type': 'wait', 'time': 3}},
    "wait_10sec": {'standby': {'type': 'wait', 'time': 10}},

    "sms_jo_route1": {'message': {'type': 'sms', 'phone_num': CELL_JO, 'text': SMS_TEXT['route1']}},
    "sms_jer_route1": {'message': {'type': 'sms', 'phone_num': CELL_JER, 'text': SMS_TEXT['route1']}}
}

ROUTE = {
    1: [
        TASK_LIST['close_doors'],
        [TASK_LIST['white_light'], TASK_LIST['goal1']],
        [TASK_LIST['goal2'], TASK_LIST['green_light']],
        [TASK_LIST['blue_light'], TASK_LIST['open_door_1']],
        TASK_LIST['wait_3sec'],
        TASK_LIST['close_door_1'],
        [TASK_LIST['goal3'], TASK_LIST['green_light']],
        [TASK_LIST['blue_light'], TASK_LIST['open_door_3']],
        TASK_LIST['wait_3sec'],
        TASK_LIST['close_door_3'],
        [TASK_LIST['goal4'], TASK_LIST['green_light']],
        TASK_LIST['sms_jer_route1']
    ],

    2: [
        TASK_LIST['close_doors'],
        [TASK_LIST['white_light'], TASK_LIST['goal2']],
        [TASK_LIST['goal1'], TASK_LIST['orange_light']],
        [TASK_LIST['violet_light'], TASK_LIST['open_door_2']],
        TASK_LIST['wait_3sec'],
        TASK_LIST['close_door_2'],
        [TASK_LIST['goal4'], TASK_LIST['orange_light']],
        TASK_LIST['blue_light']
    ],

    3: [
        TASK_LIST['close_doors'],
        [TASK_LIST['white_light'], TASK_LIST['goal3']],
        [TASK_LIST['goal5'], TASK_LIST['green_light']],
        [TASK_LIST['violet_light'], TASK_LIST['open_door_1']],
        TASK_LIST['wait_3sec'],
        TASK_LIST['close_door_1'],
        [TASK_LIST['goal6'], TASK_LIST['green_light']],
        [TASK_LIST['violet_light'], TASK_LIST['open_door_3']],
        TASK_LIST['wait_3sec'],
        TASK_LIST['close_door_3'],
        [TASK_LIST['goal1'], TASK_LIST['green_light']],
        [TASK_LIST['wait_3sec'], TASK_LIST['violet_light']],
        [TASK_LIST['goal2'], TASK_LIST['green_light']],
        TASK_LIST['orange_light']
    ],
    4: [
        TASK_LIST['close_doors'],
        [TASK_LIST['white_light'], TASK_LIST['goal1']],
        [TASK_LIST['goal2'], TASK_LIST['green_light']],
        [TASK_LIST['blue_light'], TASK_LIST['open_door_2']],
        TASK_LIST['wait_3sec'],
        TASK_LIST['close_door_2'],
        [TASK_LIST['goal3'], TASK_LIST['green_light']],
        [TASK_LIST['blue_light'], TASK_LIST['open_door_3']],
        TASK_LIST['wait_3sec'],
        TASK_LIST['close_door_3'],
        [TASK_LIST['goal4'], TASK_LIST['green_light']],
        [TASK_LIST['yellow_light'], TASK_LIST['wait_3sec']],
        [TASK_LIST['goal5'], TASK_LIST['green_light']],
        [TASK_LIST['blue_light'], TASK_LIST['open_door_1']],
        TASK_LIST['wait_3sec'],
        TASK_LIST['close_door_1'],
        [TASK_LIST['goal6'], TASK_LIST['green_light']],
        TASK_LIST['yellow_light']
    ]
}


class Controller:
    def __init__(self):
        # Controller_objects
        self.sms = SmsClient()
        self.phone = PhoneCallClient()
        # Controller_variables
        self.tasks_list = []
        self.sleeping_obligation = []
        self.current_task = None
        self.index = 0
        self.joke_key = 0
        self.main_route_thread = threading.Thread(target=self.__integral_task_processing)
        self.recorded_point_list = []  # {'id': '', 'coord': ''}
        self.actual_location = ''
        self.actual_status = ''
        self.localisationScore = None
        self.active_doors = {'opening': '', 'closing': ''}

        # Temporary Controller Variables

        # Subscribers
        self.panth_restart_sub = rospy.Subscriber('panth_delivery_goal', String, self.__restart_cb, queue_size=5)
        self.ui_command_sub = rospy.Subscriber('ui_general_cmd', String, self.__user_ui_cb, queue_size=10)
        self.estop_sub = rospy.Subscriber('estop', Bool, self.__estop_cb, queue_size=10)
        self.battery_sub = rospy.Subscriber('omron_base_SoC', String, self.__battery_event_cb, queue_size=10)
        self.location_sub = rospy.Subscriber('omron_base_location', String, self.__location_cb, queue_size=10)
        self.status_sub = rospy.Subscriber('omron_base_status', String, self.__status_cb, queue_size=10)
        self.localization_score_sub = rospy.Subscriber('omron_base_localisationScore', String,
                                                       self.__localizationScore_cb, queue_size=10)

        # Publishers
        self.panth_list_goal_pub = rospy.Publisher("panth_list_goals", pantheraGoalsList, queue_size=5)
        self.ui_text_pub = rospy.Publisher('travelling_messages', travellingMsg, queue_size=10)
        self.tasks_follow_up = rospy.Publisher('tasks_follow_up', String,
                                               queue_size=5)  # les tasks devraient avoir leur nom, actions et état

        # Services
        self.arcl_service = rospy.ServiceProxy('arcl_services', arclSrv)
        self.ledSrvColor_service = rospy.ServiceProxy('led_set_color', ledSrvColor)
        self.ledSrvRGBColor_service = rospy.ServiceProxy('led_set_rgb_color', ledSrvRGBColor)
        self.ledSrvBlinking_service = rospy.ServiceProxy('led_set_blinking', ledSrvBlinking)
        self.ledSrvPeriod_service = rospy.ServiceProxy('led_set_period_ms', ledSrvPeriod)
        self.ledSrvMultiple_service = rospy.ServiceProxy('led_set_multiple', ledSrvMultiple)
        self.doorSrvTorqueAllowed_service = rospy.ServiceProxy('door_torque_allowed_service', doorSrvTorqueAllowed)

        # Actions
        self.action_go_to_client = actionlib.SimpleActionClient('arcl_goto_action', gotoActionAction)
        self.speaking_client = actionlib.SimpleActionClient('sound_action', soundActionAction)
        self.action_open_door_client = actionlib.SimpleActionClient('open_door_action', doorActionAction)
        self.action_close_door_client = actionlib.SimpleActionClient('close_door_action', doorActionAction)
        self.action_door_calibration_client = actionlib.SimpleActionClient('door_calibration_action',
                                                                           doorCalibrationActionAction)
        self.action_robot_standby_client = actionlib.SimpleActionClient('timer_action', timerActionAction)

        # Messages
        self.ui_text = travellingMsg()

        # Flags
        self.estop = False
        self.pause = False
        self.task_cancelled = False
        self.pause_after_result = False
        self.route_replaced = False
        self.controller_idle = True
        self.enable_looping = False

    def begin(self):
        if self.speaking_client.wait_for_server():
            rospy.loginfo(f'-----[CONTROL] ***** speaking_client ready *****-----')

        if self.action_go_to_client.wait_for_server():
            rospy.loginfo(f'-----[CONTROL] ***** goto_client ready *****-----')

        if self.action_robot_standby_client.wait_for_server():
            rospy.loginfo(f'-----[CONTROL] ***** standby_client ready *****-----')

        if rosnode_ping('robot_doors_node', 5):
            if self.action_door_calibration_client.wait_for_server():
                rospy.loginfo(f'-----[CONTROL] ***** door_calibration_client ready *****-----')
            if self.action_open_door_client.wait_for_server() and self.action_close_door_client.wait_for_server():
                rospy.loginfo(f'-----[CONTROL] ***** open/close_door_client ready *****-----')
        else:
            rospy.loginfo(f'-----[CONTROL] ***** No door on this robot ***** -----')

        self.wait_for_sub_to_publisher(self.ui_text_pub)
        self.main_route_thread.start()

        time.sleep(1)

    def __integral_task_processing(self):
        rospy.loginfo(f'-----[CONTROL] ***** Main loop starting *****-----')
        # self.pause = True
        while not rospy.is_shutdown():
            if not self.pause and not self.sleeping_obligation and not self.estop and len(self.tasks_list) != 0:
                self.controller_idle = False

                self.current_task = self.tasks_list[self.index]
                result = self.__run_task()
                self.__handles_result(result)

                self.controller_idle = True
            time.sleep(0.3)

    def __run_task(self):
        rospy.loginfo(f'-----[CONTROL][START] ***** Starting task # {self.index} *****-----')
        self.current_task = self.__convert_to_list(self.current_task)
        for action in self.current_task:
            for action_type, value in action.items():
                # les actions devraient avoir leur type, les détails et le résultat

                if action_type == 'navigation':
                    self.__publish_ui_text("En route vers :", text=str(value['id']),
                                           titlecolor="black", textcolor='black')
                    goto_action = self.create_goto_action(value)
                    if value['type'] == 'point':
                        goto_action.x, goto_action.y, goto_action.heading = self.__parse_gotopoint_coord(goto_action)
                    self.action_go_to_client.send_goal(goto_action)

                elif action_type == 'sound':
                    if value['type'] == 'speak':
                        speak_action = self.create_speak_action(value['text'])
                        self.speaking_client.send_goal(speak_action)
                    elif value['type'] == 'audio_file':
                        # This is an example for an audio file
                        pass

                elif action_type == 'door':
                    # TODO: Modifier l'affichage du texte pour qu'il affiche les portes en action
                    if value['type'] == 'open':
                        door_action = self.create_door_action(value)
                        self.action_open_door_client.send_goal(door_action)
                        self.__publish_ui_text(title="Nombre de portes à ouvrir : ",
                                               text=str(len(value['id']) - value['id'].count(' ')),
                                               titlecolor="black", textcolor='black')
                    elif value['type'] == 'close':
                        door_action = self.create_door_action(value)
                        self.action_close_door_client.send_goal(door_action)
                        self.__publish_ui_text(title="Nombre de portes à fermer : ",
                                               text=str(len(value['id']) - value['id'].count(' ')),
                                               titlecolor="black", textcolor='black')
                    elif value['type'] == 'calibration':
                        self.action_door_calibration_client.send_goal()

                elif action_type == 'led':
                    self.__create_led_service_call(value)

                elif action_type == 'standby':
                    standby_action = self.create_standby_action(value)
                    self.action_robot_standby_client.send_goal(standby_action)
                    self.__publish_ui_text("En attente : ", text=str(value['time']) + ' secondes',
                                           titlecolor="black", textcolor='black')

                elif action_type == 'message':
                    # TODO: Changer pour un service au lieu d'initialiser la classe dans le controller
                    if value['type'] == 'sms':
                        self.sms.send_msg(value['phone_num'], value['text'])
                    elif value['type'] == 'voice_call':
                        self.phone.voice_call(value['phone_num'], value['text'])

                else:
                    rospy.logwarn(f'----[CONTROL]**** This action cannot be handle by controller: {action} ****----')
                    break
        return self.__wait_for_actions_result()

    def __wait_for_actions_result(self):
        results = []
        self.current_task = self.__convert_to_list(self.current_task)
        for action in self.current_task:
            for action_type, value in action.items():
                result = {'success': None, 'details': ''}

                if action_type == 'navigation':
                    self.action_go_to_client.wait_for_result()
                    result['success'] = True if self.action_go_to_client.get_state() in (2, 3) else False
                    result['details'] = self.action_go_to_client.get_result().details

                elif action_type == 'sound':
                    self.speaking_client.wait_for_result()
                    result['success'] = True if self.speaking_client.get_state() in (2, 3) else False
                    result['details'] = self.speaking_client.get_result().details

                elif action_type == 'door':
                    if value['type'] == 'open':
                        self.action_open_door_client.wait_for_result()
                        result['success'] = True if self.action_open_door_client.get_state() in (2, 3) else False
                        result['details'] = self.action_open_door_client.get_result().details
                    elif value['type'] == 'close':
                        self.action_close_door_client.wait_for_result()
                        result['success'] = True if self.action_close_door_client.get_state() in (2, 3) else False
                        result['details'] = self.action_close_door_client.get_result().details
                    elif value['type'] == 'calibration':
                        self.action_door_calibration_client.wait_for_result()
                        result['success'] = True if self.action_door_calibration_client.get_state() in (2, 3) else False
                        result['details'] = self.action_door_calibration_client.get_result().details

                elif action_type == 'standby':
                    if value['type'] == 'wait':
                        self.action_robot_standby_client.wait_for_result()
                        result['success'] = True if self.action_robot_standby_client.get_state() in (2, 3) else False
                        result['details'] = self.action_robot_standby_client.get_result().details

                else:
                    continue
                results.append(result)
        if not self.estop:
            self.__publish_ui_text(clean=True)
        return results

    def __handles_result(self, results):
        # Le controlleur ne devrait pas avoir a gérer l'échec de chaque action, mais de l'ensemble de la task
        task_success = True
        for result in results:
            self.tasks_follow_up.publish(result['details'])
            if not result['success']:
                task_success = False
        self.__handles_success('task') if task_success else self.__handles_failure('task')

    def __handles_success(self):
        rospy.loginfo(f'-----[CONTROL][END] ***** Task # {self.index} was successfully done  *****-----')
        details = f'Task # {self.index} was successfully done'
        if not self.task_cancelled:
            if self.index == (len(self.tasks_list) - 1):
                self.index = 0
                if not self.enable_looping:
                    self.pause = True
            else:
                self.index += 1
            if self.pause_after_result:
                self.pause_after_result = False
                self.pause = True
        self.task_cancelled = False

        self.tasks_follow_up.publish(details)

    def __handles_failure(self, type):
        rospy.loginfo(f'-----[CONTROL][END] ***** Task # {self.index} has failed  *****-----')
        details = ''
        if type == 'task':
            details = f'Task # {self.index} has failed'
            self.__pause_action()
        elif type == 'route':  # Not implemented yet
            details = f'ITHQ route has failed'
            self.__restart_route()
        self.tasks_follow_up.publish(details)

    def __battery_event_cb(self, battery_lvl):
        battery_lvl = float(battery_lvl.data)
        if battery_lvl < 10:
            rospy.loginfo(f"----- [BATTERY] ***** BATTERY LOW ***** -----")
            self.__dock()
            self.sleeping_obligation.append('battery')
            self.__publish_ui_text(title="Niveau de batterie faible", text=" ", titlecolor='red')
        elif 30 < battery_lvl <= 50:
            if 'battery' in self.sleeping_obligation:
                rospy.loginfo(f"----- [BATTERY] ***** BATTERY AT ACCEPTABLE LEVEL ***** -----")
                self.sleeping_obligation.remove('battery')
        else:
            if 'battery' in self.sleeping_obligation:
                self.sleeping_obligation.remove('battery')

    def __estop_cb(self, estop_on: Bool):
        if estop_on.data:
            rospy.loginfo(f"----- [ESTOP] ***** ESTOP PRESSED ***** -----")
            self.__pause_action()
            self.estop = True
            self.__publish_ui_text(title="ARRÊT D'URGENCE ENFONCÉ", text=' ', titlecolor='red')
            self.__create_led_service_call({'type': 'color', 'color': 'red'})
        else:
            if self.estop:
                self.__publish_ui_text(clean=True)
                self.__create_led_service_call({'type': 'color', 'color': 'white'})
                rospy.loginfo(f"----- [ESTOP] ***** ESTOP RELEASED ***** -----")
                self.estop = False

    def __user_ui_cb(self, command):
        command = command.data
        data = ""
        if len(command.split()) > 1:  # Separate additional data if needed
            command_parsed = command.split()
            data = command.split(' ', 1)[1]
            command = command_parsed[0]
        rospy.loginfo(f"----- [UI_user] ***** {command} ***** -----")

        if 'dock' in command:
            self.__dock()

        if 'play' in command:
            self.pause = False

        if 'pause' in command:
            self.__pause_action()

        if 'localize' in command:
            if PASSWORD_FOR_LOCALIZATION in data:
                self.arcl_service(f'localizetopoint {LOCALIZATION_POINT}')

        if 'joke' in command:
            # SPECIAL ONE, n'est pas blocable par pause
            task = {'sound': {'type': 'speak', 'text': BLAGUES.get(self.joke_key)}}
            self.joke_key += 1
            self.joke_key = self.joke_key % len(BLAGUES)
            self.__alter_tasks_list(task, offset=1)

        if 'openDoor' in command:
            self.__check_door_action_status()
            door_action = self.create_door_action({'type': 'open', 'id': data})
            self.action_open_door_client.send_goal(door_action)

        if 'closeDoor' in command:
            self.__check_door_action_status()
            door_action = self.create_door_action({'type': 'close', 'id': data})
            self.action_close_door_client.send_goal(door_action)

        if 'stopDoorMotion' in command:
            if self.action_open_door_client.get_state() == 1:
                # self.action_open_door_client.cancel_goal()
                self.action_open_door_client.send_goal(doorActionGoal())
            if self.action_close_door_client.get_state() == 1:
                # self.action_close_door_client.cancel_goal()
                self.action_close_door_client.send_goal(doorActionGoal())

        if 'setColor' in command:
            self.__create_led_service_call({'type': 'color', 'color': data})

        if 'setRGBColor' in command:
            r, g, b = map(int, data.split())
            data = [r, g, b]
            data_is_fine = True
            for color in data:
                if not (0 <= color <= 255) and isinstance(color, int):
                    data_is_fine = False
            if data_is_fine:
                self.__create_led_service_call({'type': 'color', 'color': 'solid'})
                self.__create_led_service_call({'type': 'rgb_color', 'r': r, 'g': g, 'b': b})
            else:
                self.__publish_ui_text(title="Les couleurs doivent être des entiers situés entre 0 et 255", text=" ",
                                       titlecolor="black")
                rospy.logwarn(f'----- [UI_user] ***** Les couleurs doivent être des entiers situés entre 0 et 255 '
                              f'***** -----')

        if 'saveGoal' in command:
            # TODO: Verifier s'il y a deja un goal avec ce nom dans la liste (Offrir l'option de remplacer dans le UI)
            # data est le nom du goal qui est send par le UI
            goal_id_already_used = False
            new_goal = {'id': data, 'coord': self.actual_location}
            for goal in self.recorded_point_list:
                if goal['id'] == new_goal['id']:
                    goal_id_already_used = True
            if not goal_id_already_used:
                self.recorded_point_list.append(new_goal)
                rospy.loginfo(f'----- [UI_user] ***** The goal id [{new_goal["id"]}] have been saved in the list '
                              f'***** -----')
            else:
                rospy.logwarn(f'----- [UI_user] ***** The goal id [{new_goal["id"]}] is already used ***** -----')

        if 'removeSavedGoal' in command:
            # Not fully tested
            for point in self.recorded_point_list:
                # data est le nom du goal qui est send par le UI
                if data in point['id']:
                    self.recorded_point_list.remove(point)
                else:
                    rospy.logwarn(f'----- [UI_user] *****This goal is not in the list of recorded points! ***** -----')

        if 'addGoalToTask' in command:
            # TODO: Need to add the position in the task where the new goal is added (UI parameter)
            goal_found = False
            for goal in self.recorded_point_list:
                if goal['id'] == data:
                    goal_found = True
                    self.__alter_tasks_list({'navigation': {'type': 'point',
                                                            'id': goal['id'],
                                                            'coord': goal['coord']}})
                    rospy.loginfo(f'----- [UI_user] ***** Goal added to list : {goal} ***** -----')

            if not goal_found:
                rospy.logwarn(f'----- [UI_user] ***** The goal is not in the list ***** -----')

        if 'setgoal' in command:
            self.__replace_current_route([{'navigation': {'type': 'goal', 'id': data}}])
            # self.__publish_ui_text("En route vers :", text=str(data), titlecolor="black", textcolor='black')

        if 'replaceRoute' in command:
            routes = []
            looping = False

            if '5' in data:
                looping = True
                for i in range(1, 5):
                    routes = routes + ROUTE[i] + [TASK_LIST['wait_3sec']]
            else:
                routes = ROUTE[int(data)]
            self.__replace_current_route(routes, looping)

        if 'cancelGoal' in command:
            self.__replace_current_route([])
            self.__publish_ui_text(clean=True)

        if command in ('sendSMS', 'sendVoiceCall'):
            data_parsed = data.split()
            phone_num = data_parsed[0]
            text = data.replace(phone_num, "")
            if command == 'sendSMS':
                self.sms.send_msg(phone_num, text)
            elif command == 'sendVoiceCall':
                self.phone.voice_call(phone_num, text)

        if 'speak' in command:
            speak_action = self.create_speak_action(data)
            self.speaking_client.send_goal(speak_action)

    def __location_cb(self, location):
        self.actual_location = location.data

    def __status_cb(self, status):
        self.actual_status = status

    def __localizationScore_cb(self, localizationscore):
        self.localisationScore = float(localizationscore.data)
        if self.localisationScore < 0.1:
            rospy.loginfo(f'----- [ROBOT] ***** Robot is under 10% of localization score : {self.localisationScore}'
                          f' ***** -----')
            self.__cancel_current_task()
            if 'Robot lost' not in self.sleeping_obligation:
                self.sleeping_obligation.append('Robot lost')
                self.__publish_ui_text(title='Robot lost', titlecolor='red', text=' ')
        else:
            if 'Robot lost' in self.sleeping_obligation:
                self.sleeping_obligation.remove('Robot lost')
                self.__publish_ui_text(clean=True)

    def __restart_cb(self, restart_query):
        rospy.loginfo(f"----- [UI_user] ***** Restart query ***** -----")
        self.__restart_route()

    def __restart_route(self):
        self.__cancel_current_task()
        self.__clear_tasks_list()
        # self.__fill_tasks_list()
        self.__pause_action()
        self.index = 0

    def __replace_current_route(self, task_list, looping=False):
        # Utile lorsqu'on veut commencer une tache avec l'appuis d'un bouton sur le UI
        if not self.controller_idle:
            self.__cancel_current_task()

        self.pause = False
        self.tasks_list = task_list
        self.index = 0
        self.enable_looping = looping

    def __pause_action(self):
        self.pause = True
        self.__cancel_current_task()

    def __cancel_current_task(self):
        # TODO: Might need to change for the new architecture
        rospy.loginfo(f'----- [CONTROL] ***** The current task has been cancelled ***** -----')
        if self.current_task:
            self.current_task = self.__convert_to_list(self.current_task)
            for action in self.current_task:
                for action_type, value in action.items():

                    if action_type == 'navigation':
                        if self.action_go_to_client.get_state() == 1:
                            self.action_go_to_client.cancel_goal()

                    elif action_type == 'sound':
                        if self.action_go_to_client.get_state() == 1:
                            self.speaking_client.cancel_goal()

                    elif action_type == 'door':
                        if value['type'] == 'open' and self.action_open_door_client.get_state() == 1:
                            self.action_open_door_client.send_goal(doorActionGoal())
                        elif value['type'] == 'close' and self.action_close_door_client.get_state() == 1:
                            self.action_close_door_client.send_goal(doorActionGoal())
                        elif value['type'] == 'calibration' and self.action_door_calibration_client.get_state() == 1:
                            self.action_door_calibration_client.cancel_goal()

                    elif action_type == 'standby':
                        if value['type'] == 'wait' and self.action_robot_standby_client.get_state() == 1:
                            self.action_robot_standby_client.cancel_goal()

                self.task_cancelled = True

    def __dock(self):
        # TODO: il manque a mettre un mutex lock pour ne pas commencer une task dans la loop principale
        # Si on empêche de commencer une nouvelle task, il faudrait un moyen d'annuler le docking (ex : bouton sur UI)

        self.__replace_current_route([{'navigation': {'type': 'dock', 'id': 'dock'}}])
        # self.__publish_ui_text("En route vers la station de recharge", " ", "black")

    def __alter_tasks_list(self, task, offset=0):
        # Il pourrait y avoir un problème, c'est peut-être index +1
        self.tasks_list.insert(self.index + offset, task)

    def __clear_tasks_list(self):
        self.tasks_list = []

    def __fill_tasks_list(self):
        # Ici il pourrait y avoir un query dans la base de donnee
        self.tasks_list = []

    def __publish_ui_text(self, title='', text='', titlecolor='', textcolor='', clean=False):
        if title:
            self.ui_text.title = title
        if text:
            self.ui_text.text = text
        if titlecolor:
            self.ui_text.titlecolor = titlecolor
        if textcolor:
            self.ui_text.textcolor = textcolor

        self.ui_text.clean = clean
        self.ui_text_pub.publish(self.ui_text)

    def __create_led_service_call(self, command):
        if command['type'] == 'color':
            self.ledSrvColor_service(command['color'])
        elif command['type'] == 'rgb_color':
            self.ledSrvRGBColor_service(command['r'], command['g'], command['b'])
        elif command['type'] == 'blinking':
            self.ledSrvBlinking_service(command['state'])
        elif command['type'] == 'period':
            self.ledSrvRGBColor_service(command['period'])
        elif command['type'] == 'multiple':
            self.ledSrvRGBColor_service(command['color'], command['state'], command['period'])

    def create_door_action(self, command):
        door_action = doorActionGoal()
        door_action.command = command['type']
        self.__check_door_action_status()

        for door in command['id']:

            if command['type'] == 'open':
                if door in self.active_doors['closing']:
                    self.active_doors['closing'].replace(door, '')
                if door not in self.active_doors['opening']:
                    self.active_doors['opening'] = self.active_doors['opening'] + " " + door
                door_action.door_list = self.active_doors['opening']

            if command['type'] == 'close':
                if door in self.active_doors['opening']:
                    self.active_doors['opening'].replace(door, '')
                if door not in self.active_doors['closing']:
                    self.active_doors['closing'] = self.active_doors['closing'] + " " + door
                door_action.door_list = self.active_doors['closing']

        return door_action

    def __check_door_action_status(self):
        if self.action_open_door_client.get_state() != 1:
            self.active_doors['opening'] = ""
        if self.action_close_door_client.get_state() != 1:
            self.active_doors['closing'] = ""

    @staticmethod
    def node_shutdown():
        """
        This method informs the developer about the shutdown of this node
        """
        rospy.loginfo("----- [CONTROL] ***** Node is shutting down ***** -----")

    @staticmethod
    def __parse_gotopoint_coord(goto_action):
        info = goto_action.goal.split()
        return int(info[0]), int(info[1]), int(info[2])

    @staticmethod
    def __convert_to_list(data):
        if not isinstance(data, list):
            data = [data]
        return data

    @staticmethod
    def create_goto_action(command):
        action_goto = gotoActionGoal()

        if command['type'] == 'dock':
            action_goto.command = command['type']

        elif command['type'] == 'goal':
            action_goto.command = 'goto'
            action_goto.goal = command['id']

        elif command['type'] == 'point':
            action_goto.command = 'gotoPoint'
            action_goto.goal = command['coord']

        return action_goto

    @staticmethod
    def create_speak_action(command):
        speak_action = soundActionGoal()
        speak_action.type = 'voice'
        speak_action.info = command
        return speak_action

    @staticmethod
    def create_standby_action(command):
        standby_action = timerActionGoal()
        standby_action.time = command['time']
        if 'stoppable' in command:
            standby_action.is_stoppable = command['stoppable']
        return standby_action

    @staticmethod
    def wait_for_sub_to_publisher(publisher: rospy.Publisher, timeout=0):
        index = 0
        while publisher.get_num_connections() <= 0:
            time.sleep(0.2)
            if timeout:
                index += 1
                if index >= timeout * 5:
                    return False
        return True


if __name__ == "__main__":
    try:
        rospy.init_node(os.path.splitext(os.path.basename(__file__))[0])
        node = Controller()
        rospy.on_shutdown(node.node_shutdown)
        node.begin()
        rospy.spin()
        # signal.signal(signal.SIGINT, node.signal_handler)
    except Exception:
        rospy.logerr(traceback.format_exc())

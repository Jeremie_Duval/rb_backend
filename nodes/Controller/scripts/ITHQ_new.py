#!/usr/bin/env python

import os
import csv
import glob
import time
import rospy
import datetime
import actionlib
import threading
import traceback
from std_msgs.msg import String, Bool, Empty
from comm_protocols_drivers.telnet_client import TelnetOmron
from omron_base_nodes.srv import arclSrv
from omron_base_nodes.msg import gotoActionAction, gotoActionGoal
from ui_user_nodes.msg import pantheraGoalsList, pantheraTravellingMsg
from ui_user_nodes.msg import timerActionAction, timerActionGoal
from sound_package.msg import soundActionAction, soundActionGoal

TEXTE_DEBUT = ("Bonjour! Bienvenue à l’ I Thé ash cul, votre nouvelle école!", 5)
DIRECTION_ACCUEIL=("Suivez-moi, je vais vous escorter et vous indiquer le chemin pour vous rendre à la salle où se tiendra votre accueil.", 8)
ARRET_ACCUEIL=("Nous nous trouvons présentement dans le all de l’hôtel... Saviez-vous que notre hôtel abrite 42 chambres et qu’elles sont un lieu d’apprentissage?... L’hôtel est présentement en rénovations majeures. Vous recevrez éventuellement des nouvelles concernant sa Ré Ouverture! À votre droite se trouve le comptoir de réception de l’hôtel, là où nous accueillons les clients.", 23 )
RETOURNE_ACCUEIL=("Derrière moi, l’escalier, B..1. Vous devez emprunter cet escalier pour accéder aux vestiaires des femmes.... Veuillez me suivre....",9)
MARCHE_VERS_ESCALIER= ("... ... Nous entrons maintenant dans la partie école. À votre droite, le bureau de la sécurité. Si jamais vous vous blessez ou si vous perdez un objet personnel, présentez-vous ici et un agent de sécurité pourra vous venir en aide.... Continuons", 16)
ARRET_ESCALIER=("Au bout du couloir, vous pourrez y découvrir votre Café étudiant, un endroit où vous pouvez aller « tchillé» ou manger... Quant aux vestiaires des hommes, ils sont complètement à gauche, après le Café étudiant....  En terminant, laissez-moi vous parler d’un simple principe de l’accueil que nous adoptons ici à l’école...  lorsqu’on croise quelqu’un à 3 mètres de nous, on fait un signe de la tête!... Et à 1 mètre, on dit bonjour. Une petite règle d’or qu’on vous invite à pratiquer!",33)
RETOURNE_VERS_ESCALIER=(" C’est ici que se termine cette petite visite. Si cela peut vous être utile dans votre organisation scolaire, je vous invite à prendre un agenda. Il est maintenant temps de prendre les escaliers roulants jusqu’au 2e étage. Je vous souhaite une belle rencontre et surtout, une belle session!",35)
FIN=("Si vous voulez rire un peu, n'hésitez pas à péser sur le champignon de mon écran ...hihihihi!", 7)

G_DEBUT = "gkt"
G_ACCUEIL = "ytkty"
G_ACCUEIL_RETURN = "tykt7y"
G_TEXT = "ygu,ytg"
G_ESCALIER = "café"
G_ESCALIER_RETURN = "escaliers"
G_360_TURN = "fin_sequence"

BLAGUES = {0:("C'est l'histoire d'un aveugle qui rentre dans un bar....Et dans une table, et dans une chaise, et dans un mur...", 5),
           1:("C'est quoi une chauve souris avec une perruque ? ... Une souris.", 5),
           2:("Qu'est ce qui n'est pas un steak ? ...Une pastèque.", 5),
           3:("Une fesse gauche rencontre une fesse droite : .... « Tu ne trouves pas que ça pue dans le couloir ? »", 5),
           4:("Comment s'appelle les fesses d'un Schtroumpf ?.... Un blue-ré.", 5),
           5:("C'est l'histoire de 2 patates qui traversent la route... L’une d’elle se fait écraser. L’autre dit : « Oh purée ! »", 5),
           6:("Pourquoi faut-il enlever ses lunettes avant un alcootest ? ... Ca fait 2 verres en moins.", 5),
           7:("Comment appelle-t-on un chien qui n'a pas de pattes ? ... On ne l’appelle pas, on va le chercher ...", 5),
           8:("Que fait un crocodile quand il rencontre une superbe femelle ?...Il Lacoste.", 5),
           9:("Pourquoi les vaches ferment_elles les yeux pendant la traite de lait ?... Pour faire du lait concentré.", 5),
           10:("Quel est le point commun entre les mathématiques et le sexe ?... Plus il y a d’inconnues, plus c’est chaud.", 5),
           11:("2 vaches discutent... :  Ça te fait pas peur toi ces histoires de « vache folle » ...? Ben j’m’en fous j’suis un lapin !", 5),
           12:("Tu connais l'histoire du lit superposé ? ...C’est une histoire à dormir debout.", 5)}

TASK1 = {'navigation': G_DEBUT}
TASK2 = {'skip': TEXTE_DEBUT}
TASK3 = {'navigation': G_ACCUEIL, 'skip': DIRECTION_ACCUEIL}
TASK4 = {'skip': ARRET_ACCUEIL}
TASK5 = {'navigation': G_ACCUEIL_RETURN}
TASK6 = {'skip': RETOURNE_ACCUEIL}
TASK7 = {'navigation': G_TEXT}
TASK8 = {'navigation': G_ESCALIER, 'skip': MARCHE_VERS_ESCALIER}
TASK9 = {'skip': ARRET_ESCALIER}
TASK10 = {'navigation': G_ESCALIER_RETURN}
TASK11 = {'skip': RETOURNE_VERS_ESCALIER}
TASK12 = {'skip': FIN}


class ITHQController:
    def __init__(self):
        # Controller_variables
        # self.tasks_list = [TASK1, TASK2, TASK3, TASK4, TASK5, TASK6, TASK7, TASK8, TASK9, TASK10, TASK11]
        self.tasks_list = [TASK1]
        self.sleeping_obligation = []
        self.current_task = None
        self.index = 0
        self.joke_key = 0
        self.main_route_thread = threading.Thread(target=self.__integral_task_processing)
        self.recorded_point_list = []
        self.actual_location = ''

        # Temporary Controller Variables

        # Subscribers
        self.panth_restart_sub = rospy.Subscriber('panth_delivery_goal', String, self.__restart_cb, queue_size=5)
        self.ui_command_sub = rospy.Subscriber('ui_general_cmd', String, self.__user_ui_cb, queue_size=10)
        self.estop_sub = rospy.Subscriber('estop', Bool, self.__estop_cb, queue_size=10)
        self.battery_sub = rospy.Subscriber('omron_base_SoC', String, self.__battery_event_cb, queue_size=10)
        self.location_sub = rospy.Subscriber('omron_base_location', String, self.__location_cb, queue_size=10)

        # Publishers
        self.panth_list_goal_pub = rospy.Publisher("panth_list_goals", pantheraGoalsList, queue_size=5)
        self.ui_text_pub = rospy.Publisher('panth_text', pantheraTravellingMsg, queue_size=10)
        self.tasks_follow_up = rospy.Publisher('tasks_follow_up', String,
                                               queue_size=5)  # les tasks devraient avoir leur nom, les actions et leur état

        # Actions
        self.action_go_to_client = actionlib.SimpleActionClient('arcl_goto_action', gotoActionAction)
        self.speaking_client = actionlib.SimpleActionClient('sound_action', soundActionAction)

        # Messages
        self.ui_text = pantheraTravellingMsg()
        self.ui_timer = timerActionGoal()
        self.goto_action = gotoActionGoal()
        self.speak_action = soundActionGoal()

        # Flags
        self.estop = False
        self.pause = False
        self.pause_after_result = False

    def begin(self):
        if self.speaking_client.wait_for_server():
            rospy.loginfo(f'-----[CONTROL] ***** speaking_client ready *****-----')

        if self.action_go_to_client.wait_for_server():
            rospy.loginfo(f'-----[CONTROL] ***** goto_client ready *****-----')

        wait_for_sub_to_publisher(self.ui_text_pub)
        self.main_route_thread.start()

        time.sleep(1)

    def __integral_task_processing(self):
        rospy.loginfo(f'-----[CONTROL] ***** Main loop starting *****-----')
        self.pause = True
        self.__publish_ui_text("Bienvenue à l'ITHQ!", 'black', "Veuillez appuyer sur le bouton 'play' pour commencer", 'green')
        while not rospy.is_shutdown():
            if not self.pause and not self.sleeping_obligation and not self.estop and len(self.tasks_list) != 0:
                self.current_task = self.tasks_list[self.index]
                result = self.__run_task()
                self.__handles_result(result)
                if self.pause_after_result:
                    self.pause = True
                    self.pause_after_result = False
            time.sleep(0.3)

    def __run_task(self):
        rospy.loginfo(f'-----[CONTROL][START] ***** Starting task # {self.index} *****-----')
        for action, value in self.current_task.items():
            # les actions devraient avoir leur type, les détails et le résultat
            if action == 'navigation':
                goto_action = create_goto_action(value)
                self.action_go_to_client.send_goal(goto_action)
                self.__publish_ui_text(text2=f'Destination: {value}', color2='gray')
            if action == 'navigationToPoint':
                goto_action = create_goto_action(value)
                goto_action.x, goto_action.y, goto_action.heading = self.__parse_gotopoint_info(goto_action)
                self.action_go_to_client.send_goal(goto_action)
                for point in self.recorded_point_list:
                    if point[1] == value:
                        self.__publish_ui_text(text2=f'Destination: {point[0]}', color2='gray')
            elif action == 'speaking':
                speak_action = create_speak_action(value)
                self.speaking_action = self.speaking_client.send_goal(speak_action)
            else:
                rospy.logwarn(f'-----[CONTROL]***** This action cannot be handle by controller {action} *****-----')
                break

        return self.__wait_for_actions_result()

    def __wait_for_actions_result(self):
        results = []
        for action, value in self.current_task.items():
            result = {'success': None, 'details': ''}
            if action in ('navigation', 'navigationToPoint'):
                self.action_go_to_client.wait_for_result()
                result['success'] = True if self.action_go_to_client.get_state() == 3 else False
                result['details'] = self.action_go_to_client.get_result().details
            if action == 'speaking':
                self.speaking_client.wait_for_result()
                result['success'] = True if self.speaking_client.get_state() == 3 else False
                result['details'] = self.speaking_client.get_result().details  # Ce devrait etre une action puisqu'elle dure plus d'une seconde
            else:
                continue
            results.append(result)
        return results

    def __pause_action(self):
        # TODO : Il faut éviter que le robot entame sa prochaine task lorsque la task en cours est annulée
        # Trouver un meilleur moyen de garder la task en mémoire que seulement ajouter au début la même task
        self.pause = True
        self.__cancel_current_task()
        self.__alter_tasks_list(self.current_task)

    def __cancel_current_task(self):
        if self.current_task is not None:
            for action in self.current_task:
                if action == 'navigation':
                    if self.action_go_to_client.get_state() == 1:
                        self.action_go_to_client.cancel_goal()
                if action == 'speaking':
                    if self.action_go_to_client.get_state() == 1:
                        self.speaking_client.cancel_goal()

    def __handles_result(self, results):
        # Le controlleur ne devrait pas avoir a gérer l'échec de chaque action, mais de l'ensemble de la task
        task_success = True
        for result in results:
            print(result)
            self.tasks_follow_up.publish(result['details'])
            if not result['success']:
                task_success = False
        self.__handles_success('task') if task_success else self.__handles_failure('task')

    def __handles_success(self, type):
        rospy.loginfo(f'-----[CONTROL][END] ***** Task # {self.index} was successfully done  *****-----')
        details = f'Task # {self.index} was successfully done'
        if self.index == (len(self.tasks_list)-1):
            self.index = 0
        else:
            self.index += 1

        # THIS IS ONLY FOR ITHQ CONTROLLER
        # if self.tasks_list[self.index] == {'speaking': TEXTE_DEBUT}:
        #     self.pause = True
        #     self.__publish_ui_text("Bienvenue à l'ITHQ!", 'black',
        #                            "Veuillez appuyer sur le bouton 'play' pour commencer", 'green')

        self.tasks_follow_up.publish(details)

    def __handles_failure(self, type):
        rospy.loginfo(f'-----[CONTROL][END] ***** Task # {self.index} has failed  *****-----')
        details = ''
        if type == 'task':
            details = f'Task # {self.index} has failed'
            self.__pause_action()
            self.__publish_ui_text(text2=details+' please press play to retry', color2='red')
        if type == 'route':
            details = f'ITHQ route has failed'
            self.__restart_route()
        self.tasks_follow_up.publish(details)

    def __schedule_task_cb(self):
        pass

    def __battery_event_cb(self, battery_lvl):
        battery_lvl = float(battery_lvl.data)
        if battery_lvl < 10:
            rospy.loginfo(f"~~~~~[BATTERY] BATTERY LOW ~~~~~~")
            self.__dock()
            self.sleeping_obligation = ['battery']
            self.__publish_ui_text('Niveau de batterie faible', 'red')
        elif 30 < battery_lvl <= 50:
            for idx, stop_obligation in self.sleeping_obligation:
                if stop_obligation == 'battery':
                    rospy.loginfo(f"~~~~~[BATTERY] BATTERY AT ACCEPTABLE LEVEL ~~~~~~")
                    self.__publish_ui_text("Bienvenue à l'ITHQ!", 'black', "Veuillez appuyer sur le bouton 'play' pour commencer", 'green')
                    self.sleeping_obligation.remove('battery')

    def __estop_cb(self, estop_on: Bool):
        if estop_on.data:
            rospy.loginfo(f"~~~~~[ESTOP] ESTOP PRESSED ~~~~~~")
            self.__pause_action()
            self.estop = True
            self.__publish_ui_text('ESTOP PRESSED', 'red')
        else:
            if self.estop:
                rospy.loginfo(f"~~~~~[ESTOP] ESTOP RELEASED ~~~~~~")
                self.estop = False
                self.__publish_ui_text("Bienvenue à l'ITHQ!", 'black')

    def __user_ui_cb(self, command):
        command = command.data
        data = ""
        if len(command.split()) > 1:  # Separate additional data if needed
            command_parsed = command.split()
            data = command.split(' ', 1)[1]
            command = command_parsed[0]
        rospy.loginfo(f"~~~~~[UI_user] {command} ~~~~~~")

        if command == 'dock':
            self.__dock()

        if command == 'play':
            self.pause = False

        if command == 'pause':
            self.__pause_action()
            self.__publish_ui_text(text2='PAUSE', color2='gray')

        if command == 'joke':
            # SPECIAL ONE, n'est pas blocable par pause
            task = {'speaking': BLAGUES.get(self.joke_key)}
            self.joke_key += 1
            self.joke_key = self.joke_key % len(BLAGUES)
            self.__alter_tasks_list(task, offset=1)

        if command == 'saveGoal':
            # TODO: Attribuer un ID (NOM) au goal lors de l'enregistrement a partir du UI (Adaptation vrai UI)
            new_goal = (data, self.actual_location)
            self.recorded_point_list.append(new_goal)
            # Ajouter une task dans la liste de task avec un goal precis
            # self.__alter_tasks_list({'navigationToPoint': self.recorded_point_list[self.goalID][1]})
            print(f'{self.recorded_point_list}')

        if command == 'addGoalToTask':
            for goal in self.recorded_point_list:
                if goal[0] == data:
                    self.__alter_tasks_list({'navigationToPoint': goal[1]})
                    print(f'Goal added to list : {goal}')
                else:
                    rospy.logwarn(f'The goal is not in the list')

    def __location_cb(self, location):
        self.actual_location = location.data

    def __restart_cb(self, restart_query):
        rospy.loginfo(f"~~~~~[UI_user] Restart query ~~~~~~")
        self.__restart_route()

    def __restart_route(self):
        self.__cancel_current_task()
        self.__clear_tasks_list()
        self.__fill_tasks_list()
        self.__pause_action()
        self.index = 0

    def __dock(self):
        # TODO: il manque a mettre un mutex lock pour ne pas commencer une task dans la loop principale
        # Si on empêche de commencer une nouvelle task, il faudrait un moyen d'annuler le docking (ex : bouton sur HMI)
        self.__restart_route()  # so when undocking, it will start from beginning
        task = {'navigation': 'dock'}
        self.__alter_tasks_list(task)
        self.pause_after_result = True

    def __alter_tasks_list(self, task, offset = 0):
        # Il pourrait y avoir un problème, c'est peut-être index +1
        self.tasks_list.insert(self.index + offset, task)

    def __clear_tasks_list(self):
        self.tasks_list = []

    def __fill_tasks_list(self):
        # Ici il pourrait y avoir un query dans la base de donnee
        # self.tasks_list = [TASK1, TASK2, TASK3, TASK4, TASK5, TASK6, TASK7, TASK8, TASK9, TASK10, TASK11]
        self.tasks_list = [TASK1]

    def __publish_ui_text(self, text1='', color1='', text2='', color2='', clean1=False, clean2=False):
        if text1:
            self.ui_text.text1 = text1
        if color1:
            self.ui_text.color1 = color1
        if text2:
            self.ui_text.text2 = text2
        if color2:
            self.ui_text.color2 = color2
        if clean1:
            self.ui_text.clean1 = clean1
        if clean2:
            self.ui_text.clean2 = clean2

        self.ui_text_pub.publish(self.ui_text)

    def node_shutdown(self):
        """
        This method informs the developper about the shutdown of this node
        """
        rospy.loginfo("node shutting down")

    @staticmethod
    def __parse_gotopoint_info(goto_action):
        info = goto_action.goal.split()
        return int(info[0]), int(info[1]), int(info[2])


def create_goto_action(command):
    action_goto = gotoActionGoal()
    if type(command) != str:  # like point x, y, heading
        # ITHQ controller doesn't handles this
        pass
    else:
        if command == 'dock':
            action_goto.command = command
            pass
        else:
            spaces_count = 0
            for char in command:
                if char == ' ':
                    spaces_count += 1
            if spaces_count == 3:
                action_goto.command = 'gotoPoint'
                action_goto.goal = command
            else:
                action_goto.command = 'goto'
                action_goto.goal = command
    return action_goto


def create_speak_action(command):
    speak_action = soundActionGoal()
    speak_action.type = 'voice'
    speak_action.info = command[0]
    speak_action.time = command[1]
    return speak_action


def wait_for_sub_to_publisher(publisher: rospy.Publisher, timeout=0):
    index = 0
    while publisher.get_num_connections() <= 0 :
        time.sleep(0.2)
        if timeout:
            index += 1
            if index >= timeout*5:
                return False

    return True


if __name__ == "__main__":
    try:
        rospy.init_node(os.path.splitext(os.path.basename(__file__))[0])
        node = ITHQController()
        rospy.on_shutdown(node.node_shutdown)
        node.begin()
        rospy.spin()
        # signal.signal(signal.SIGINT, node.signal_handler)
    except Exception:
        rospy.logerr(traceback.format_exc())


#!/usr/bin/env python

import os
import csv
import glob
import time
import rospy
import datetime
import actionlib
import threading
import traceback
from std_msgs.msg import String, Bool, Empty
from comm_protocols_drivers.telnet_client import TelnetOmron
from omron_base_nodes.srv import arclSrv
from omron_base_nodes.msg import gotoActionAction, gotoActionGoal
from ui_user_nodes.msg import pantheraGoalsList, pantheraTravellingMsg, travellingMsg
from ui_user_nodes.msg import timerActionAction, timerActionGoal
from sound_package.msg import soundActionAction, soundActionGoal

LOCALIZATION_POINT = '-1600 7300 90'
PASSWORD_FOR_LOCALIZATION = '1234'

TEXT_ACCUEIL_ET_SALLE_DE_CONFERENCE = "Vous êtes maintenant arrivés à l'accueil, et à la salle de conférence!"
TEXT_SALLE_DE_BAIN = "Vous êtes maintenant arrivés à la salle de bain!"
TEXT_CUISINE_ET_CAFE = "Vous êtes maintenant arrivés à la zone cuisine, et café!"
TEXT_ZONE_DECOUVERTE = "Vous êtes maintenant arrivés à la zone, Découverte!"
TEXT_USINE_EXPERIMENTALE = "Vous êtes maintenant arrivés à l'usine expérimentale!"
TEXT_ZONE_COLLABORATION = "Vous êtes maintenant arrivés à la zone de collaboration!"
TEXT_ZONE_REALITE_VIRTUELLE = "Vous êtes maintenant arrivés à la zone de réalité virtuelle!"
TEXT_SALLE_DE_FORMATION = "Vous êtes maintenant arrivés à la salle de formation!"
TEXT_ZONE_INTERACTION = "Vous êtes maintenant arrivés à la zone d'intéraction"

TEXT_STATION_DE_COMMANDE = "Vous êtes maintenant arrivés à la station de commande!"
TEXT_MATIERE_PREMIERE = "Vous êtes maintenant arrivés à l'emplacement des matières premières!"
TEXT_STATION_MANUELLE = "Vous êtes maintenant arrivés à la station manuelle!"
TEXT_STATION_ROBOTISEE = "Vous êtes maintenant arrivés à la station robotisée!"
TEXT_GRAVURE = "Vous êtes maintenant arrivés à la gravure!"
TEXT_PRODUITS_FINIS = "Vous êtes maintenant arrivés aux produits finis!"

BLAGUES = {0:("C'est l'histoire d'un aveugle qui rentre dans un bar....Et dans une table, et dans une chaise, et dans un mur...", 5),
           1:("C'est quoi une chauve souris avec une perruque ? ... Une souris.", 5),
           2:("Qu'est ce qui n'est pas un steak ? ...Une pastèque.", 5),
           3:("Une fesse gauche rencontre une fesse droite : .... « Tu ne trouves pas que ça pue dans le couloir ? »", 5),
           4:("Comment s'appelle les fesses d'un Schtroumpf ?.... Un blue-ré.", 5),
           5:("C'est l'histoire de 2 patates qui traversent la route... L’une d’elle se fait écraser. L’autre dit : « Oh purée ! »", 5),
           6:("Pourquoi faut-il enlever ses lunettes avant un alcootest ? ... Ca fait 2 verres en moins.", 5),
           7:("Comment appelle-t-on un chien qui n'a pas de pattes ? ... On ne l’appelle pas, on va le chercher ...", 5),
           8:("Que fait un crocodile quand il rencontre une superbe femelle ?...Il Lacoste.", 5),
           9:("Pourquoi les vaches ferment_elles les yeux pendant la traite de lait ?... Pour faire du lait concentré.", 5),
           10:("Quel est le point commun entre les mathématiques et le sexe ?... Plus il y a d’inconnues, plus c’est chaud.", 5),
           11:("2 vaches discutent... :  Ça te fait pas peur toi ces histoires de « vache folle » ...? Ben j’m’en fous j’suis un lapin !", 5),
           12:("Tu connais l'histoire du lit superposé ? ...C’est une histoire à dormir debout.", 5)}


class DigifabController:
    def __init__(self):
        # Controller_variables
        self.tasks_list = []
        self.sleeping_obligation = []
        self.current_task = None
        self.index = 0
        self.joke_key = 0
        self.main_route_thread = threading.Thread(target=self.__integral_task_processing)
        self.recorded_point_list = []
        self.actual_location = ''

        # Temporary Controller Variables

        # Subscribers
        self.panth_restart_sub = rospy.Subscriber('panth_delivery_goal', String, self.__restart_cb, queue_size=5)
        self.ui_command_sub = rospy.Subscriber('ui_general_cmd', String, self.__user_ui_cb, queue_size=10)
        self.estop_sub = rospy.Subscriber('estop', Bool, self.__estop_cb, queue_size=10)
        self.battery_sub = rospy.Subscriber('omron_base_SoC', String, self.__battery_event_cb, queue_size=10)
        self.location_sub = rospy.Subscriber('omron_base_location', String, self.__location_cb, queue_size=10)

        # Publishers
        self.panth_list_goal_pub = rospy.Publisher("panth_list_goals", pantheraGoalsList, queue_size=5)
        self.ui_text_pub = rospy.Publisher('travelling_messages', travellingMsg, queue_size=10)
        self.tasks_follow_up = rospy.Publisher('tasks_follow_up', String,
                                               queue_size=5)  # les tasks devraient avoir leur nom, les actions et leur état

        # Services
        self.arcl_service = rospy.ServiceProxy('arcl_services', arclSrv)

        # Actions
        self.action_go_to_client = actionlib.SimpleActionClient('arcl_goto_action', gotoActionAction)
        self.speaking_client = actionlib.SimpleActionClient('sound_action', soundActionAction)

        # Messages
        self.ui_text = travellingMsg()
        self.ui_timer = timerActionGoal()
        self.goto_action = gotoActionGoal()
        self.speak_action = soundActionGoal()

        # Flags
        self.estop = False
        self.pause = False
        self.task_cancelled = False
        self.task_list_finished = False
        self.dock_request = False

    def begin(self):
        if self.speaking_client.wait_for_server():
            rospy.loginfo(f'-----[CONTROL] ***** speaking_client ready *****-----')

        if self.action_go_to_client.wait_for_server():
            rospy.loginfo(f'-----[CONTROL] ***** goto_client ready *****-----')

        wait_for_sub_to_publisher(self.ui_text_pub)
        self.main_route_thread.start()

        time.sleep(1)

    def __integral_task_processing(self):
        rospy.loginfo(f'-----[CONTROL] ***** Main loop starting *****-----')
        # self.pause = True
        while not rospy.is_shutdown():
            if not self.pause and not self.sleeping_obligation and not self.estop and len(self.tasks_list) != 0:
                self.task_list_finished = False
                self.current_task = self.tasks_list[self.index]
                result = self.__run_task()
                self.__handles_result(result)
                if self.task_list_finished and not self.dock_request:
                    self.task_list_finished = False
                    self.pause = True
                self.dock_request = False
            time.sleep(0.3)

    def __run_task(self):
        rospy.loginfo(f'-----[CONTROL][START] ***** Starting task # {self.index} *****-----')
        for action, value in self.current_task.items():
            # les actions devraient avoir leur type, les détails et le résultat
            if action == 'navigation':
                goto_action = create_goto_action(value)
                self.action_go_to_client.send_goal(goto_action)
            elif action == 'navigationToPoint':
                goto_action = create_goto_action(value)
                goto_action.x, goto_action.y, goto_action.heading = self.__parse_gotopoint_info(goto_action)
                self.action_go_to_client.send_goal(goto_action)
                for point in self.recorded_point_list:
                    if point[1] == value:
                        pass
            elif action == 'speaking':
                speak_action = create_speak_action(value)
                self.speaking_action = self.speaking_client.send_goal(speak_action)
            else:
                rospy.logwarn(f'-----[CONTROL]***** This action cannot be handle by controller {action} *****-----')
                break
        return self.__wait_for_actions_result()

    def __wait_for_actions_result(self):
        results = []
        for action, value in self.current_task.items():
            result = {'success': None, 'details': ''}
            if action in ('navigation', 'navigationToPoint'):
                self.action_go_to_client.wait_for_result()
                result['success'] = True if self.action_go_to_client.get_state() == 3 else False
                result['details'] = self.action_go_to_client.get_result().details
            if action == 'speaking':
                self.speaking_client.wait_for_result()
                result['success'] = True if self.speaking_client.get_state() == 3 else False
                result['details'] = self.speaking_client.get_result().details  # Ce devrait etre une action puisqu'elle dure plus d'une seconde
            else:
                continue
            results.append(result)
        return results

    def __handles_result(self, results):
        # Le controlleur ne devrait pas avoir a gérer l'échec de chaque action, mais de l'ensemble de la task
        task_success = True
        for result in results:
            self.tasks_follow_up.publish(result['details'])
            if not result['success']:
                task_success = False
        self.__handles_success('task') if task_success else self.__handles_failure('task')

    def __handles_success(self, type):
        rospy.loginfo(f'-----[CONTROL][END] ***** Task # {self.index} was successfully done  *****-----')
        details = f'Task # {self.index} was successfully done'
        if not self.task_cancelled or self.dock_request:
            self.__publish_ui_text(clean=True)
            if self.index == (len(self.tasks_list)-1):
                self.index = 0
                self.task_list_finished = True
            else:
                self.index += 1
                self.task_list_finished = False
        self.task_cancelled = False
        if "speaking" in self.current_task:
            self.pause = True

        self.tasks_follow_up.publish(details)

    def __handles_failure(self, type):
        rospy.loginfo(f'-----[CONTROL][END] ***** Task # {self.index} has failed  *****-----')
        details = ''
        if type == 'task':
            details = f'Task # {self.index} has failed'
            self.__pause_action()
        if type == 'route':
            details = f'ITHQ route has failed'
            self.__restart_route()
        self.tasks_follow_up.publish(details)

    def __schedule_task_cb(self):
        pass

    def __battery_event_cb(self, battery_lvl):
        battery_lvl = float(battery_lvl.data)
        if battery_lvl < 10:
            rospy.loginfo(f"~~~~~[BATTERY] BATTERY LOW ~~~~~~")
            self.__dock()
            self.sleeping_obligation.append('battery')
            self.__publish_ui_text(title="Niveau de batterie faible", text=" ", titlecolor='red')
        elif 30 < battery_lvl <= 50:
            if 'battery' in self.sleeping_obligation:
                rospy.loginfo(f"~~~~~[BATTERY] BATTERY AT ACCEPTABLE LEVEL ~~~~~~")
                self.sleeping_obligation.remove('battery')
        else:
            if 'battery' in self.sleeping_obligation:
                self.sleeping_obligation.remove('battery')

    def __estop_cb(self, estop_on: Bool):
        if estop_on.data:
            rospy.loginfo(f"~~~~~[ESTOP] ESTOP PRESSED ~~~~~~")
            self.__pause_action()
            self.estop = True
            self.__publish_ui_text(title='ESTOP PRESSED', text=' ', titlecolor='red')
        else:
            if self.estop:
                self.__publish_ui_text(clean=True)
                rospy.loginfo(f"~~~~~[ESTOP] ESTOP RELEASED ~~~~~~")
                self.estop = False

    def __user_ui_cb(self, command):
        command = command.data
        data = ""
        if len(command.split()) > 1:  # Separate additional data if needed
            command_parsed = command.split()
            data = command.split(' ', 1)[1]
            command = command_parsed[0]
        rospy.loginfo(f"~~~~~[UI_user] {command} ~~~~~~")

        if command == 'dock':
            self.__dock()

        if command == 'play':
            self.pause = False

        if 'localize' in command:
            if PASSWORD_FOR_LOCALIZATION in data:
                self.arcl_service(f'localizetopoint {LOCALIZATION_POINT}')

        if command == 'pause':
            self.__pause_action()

        if command == 'joke':
            # SPECIAL ONE, n'est pas blocable par pause
            task = {'speaking': BLAGUES.get(self.joke_key)}
            self.joke_key += 1
            self.joke_key = self.joke_key % len(BLAGUES)
            self.__alter_tasks_list(task, offset=1)

        if command == 'saveGoal':
            # TODO: Attribuer un ID (NOM) au goal lors de l'enregistrement a partir du UI (Adaptation vrai UI)
            new_goal = (data, self.actual_location)
            self.recorded_point_list.append(new_goal)

        if command == 'addGoalToTask':
            # TODO: Need to add the position in the task where the new goal is added
            for goal in self.recorded_point_list:
                if goal[0] == data and self.tasks_list == []:
                    self.tasks_list = [{'navigationToPoint': goal[1]}]
                    rospy.loginfo(f'Goal added to list : {goal}')
                elif goal[0] == data:
                    self.__alter_tasks_list({'navigationToPoint': goal[1]})
                    rospy.loginfo(f'Goal added to list : {goal}')
                else:
                    rospy.logwarn(f'The goal is not in the list')

        # This command is coded for Digifab
        if command == 'setgoal':
            if not self.task_list_finished:
                self.__cancel_current_task()
            if "Usine_Experimentale" not in data:
                self.pause = False
                self.dock_request = False
                self.current_task = [{'navigation': data}]
                self.tasks_list = self.current_task
                self.__publish_ui_text("En route vers :", titlecolor="black", textcolor='black')

                if 'Zone_Accueil_et_Salle_de_Conference' in data:
                    self.__alter_tasks_list({'speaking': TEXT_ACCUEIL_ET_SALLE_DE_CONFERENCE}, offset=1)
                    self.__publish_ui_text(text="Accueil et salle de conférence")
                elif 'Salle_de_Bain' in data:
                    self.__alter_tasks_list({'speaking': TEXT_SALLE_DE_BAIN}, offset=1)
                    self.__publish_ui_text(text="Salle de bain")
                elif 'Cuisine_et_Cafe' in data:
                    self.__alter_tasks_list({'speaking': TEXT_CUISINE_ET_CAFE}, offset=1)
                    self.__publish_ui_text(text="Cuisine et café")
                elif 'Zone_Decouverte' in data:
                    self.__alter_tasks_list({'speaking': TEXT_ZONE_DECOUVERTE}, offset=1)
                    self.__publish_ui_text(text="Zone découverte")
                elif 'Usine_Experimentale' in data:
                    # self.__alter_tasks_list({'speaking': TEXT_USINE_EXPERIMENTALE}, offset=1)
                    # self.__publish_ui_text(text="Usine expérimentale")
                    pass
                elif 'Zone_Collaboration' in data:
                    self.__alter_tasks_list({'speaking': TEXT_ZONE_COLLABORATION}, offset=1)
                    self.__publish_ui_text(text="Zone de collaboration")
                elif 'Zone_Realite_Virtuelle' in data:
                    self.__alter_tasks_list({'speaking': TEXT_ZONE_REALITE_VIRTUELLE}, offset=1)
                    self.__publish_ui_text(text="Zone de réalité virtuelle")
                elif 'Salle_de_Formation' in data:
                    self.__alter_tasks_list({'speaking': TEXT_SALLE_DE_FORMATION}, offset=1)
                    self.__publish_ui_text(text="Salle de formation")
                elif 'Zone_Interaction' in data:
                    self.__alter_tasks_list({'speaking': TEXT_ZONE_INTERACTION}, offset=1)
                    self.__publish_ui_text(text="Zone d'intéraction")
                elif 'Station_de_Commande' in data:
                    self.__alter_tasks_list({'speaking': TEXT_STATION_DE_COMMANDE}, offset=1)
                    self.__publish_ui_text(text="Station de commande")
                elif 'Matieres_Premieres' in data:
                    self.__alter_tasks_list({'speaking': TEXT_MATIERE_PREMIERE}, offset=1)
                    self.__publish_ui_text(text="Matière première")
                elif 'Station_Manuelle' in data:
                    self.__alter_tasks_list({'speaking': TEXT_STATION_MANUELLE}, offset=1)
                    self.__publish_ui_text(text="Station manuelle")
                elif 'Station_Robotisee' in data:
                    self.__alter_tasks_list({'speaking': TEXT_STATION_ROBOTISEE}, offset=1)
                    self.__publish_ui_text(text="Station robotisée")
                elif 'Gravure' in data:
                    self.__alter_tasks_list({'speaking': TEXT_GRAVURE}, offset=1)
                    self.__publish_ui_text(text="Gravure")
                elif 'Produits_Finis' in data:
                    self.__alter_tasks_list({'speaking': TEXT_PRODUITS_FINIS}, offset=1)
                    self.__publish_ui_text(text="Produits finis")
                else:
                    self.__alter_tasks_list({'speaking': "Ce choix n'est pas dans la liste!"}, offset=1)

        if command == 'cancelGoal':
            self.__cancel_current_task()
            self.current_task = []
            self.tasks_list = self.current_task
            self.pause = True
            self.dock_request = False
            self.__publish_ui_text(clean=True)

    def __location_cb(self, location):
        self.actual_location = location.data

    def __restart_cb(self, restart_query):
        rospy.loginfo(f"~~~~~ [UI_user] Restart query ~~~~~~")
        self.__restart_route()

    def __restart_route(self):
        self.__cancel_current_task()
        self.__clear_tasks_list()
        self.__fill_tasks_list()
        self.__pause_action()
        self.index = 0

    def __restart_current_task(self):
        # restart la task en cours en ajoutant une task skip pour finir la loop du controller et
        # commencer directement a la premiere task

        # Utile lorsqu'on veut commencer une tache avec l'appuis d'un bouton sur le UI
        pass

    def __pause_action(self):
        # TODO : Il faut éviter que le robot entame sa prochaine task lorsque la task en cours est annulée
        # Trouver un meilleur moyen de garder la task en mémoire que seulement ajouter au début la même task
        self.pause = True
        self.__cancel_current_task()
        # self.__alter_tasks_list(self.current_task)

    def __cancel_current_task(self):
        if self.current_task:
            for action in self.current_task:
                if action in ('navigation', 'navigationToPoint'):
                    if self.action_go_to_client.get_state() == 1:
                        self.action_go_to_client.cancel_goal()
                if action == 'speaking':
                    if self.action_go_to_client.get_state() == 1:
                        self.speaking_client.cancel_goal()
            self.task_cancelled = True

    def __dock(self):
        # TODO: il manque a mettre un mutex lock pour ne pas commencer une task dans la loop principale
        # Si on empêche de commencer une nouvelle task, il faudrait un moyen d'annuler le docking (ex : bouton sur HMI)
        self.__restart_route()  # so when undocking, it will start from beginning
        self.current_task = [{'navigation': 'dock'}]
        self.tasks_list = self.current_task
        # self.__alter_tasks_list(task)
        self.pause = False
        self.dock_request = True
        self.__publish_ui_text("En route vers la station de recharge.", " ", "black")

    def __alter_tasks_list(self, task, offset=0):
        # Il pourrait y avoir un problème, c'est peut-être index +1
        self.tasks_list.insert(self.index + offset, task)

    def __clear_tasks_list(self):
        self.tasks_list = []

    def __fill_tasks_list(self):
        # Ici il pourrait y avoir un query dans la base de donnee
        self.tasks_list = []

    def __publish_ui_text(self, title='', text='', titlecolor='', textcolor='', clean=False):
        if title:
            self.ui_text.title = title
        if text:
            self.ui_text.text = text
        if titlecolor:
            self.ui_text.titlecolor = titlecolor
        if textcolor:
            self.ui_text.textcolor = textcolor

        self.ui_text.clean = clean
        self.ui_text_pub.publish(self.ui_text)

    @staticmethod
    def node_shutdown():
        """
        This method informs the developer about the shutdown of this node
        """
        rospy.loginfo("node shutting down")

    @staticmethod
    def __parse_gotopoint_info(goto_action):
        info = goto_action.goal.split()
        return int(info[0]), int(info[1]), int(info[2])


def create_goto_action(command):
    action_goto = gotoActionGoal()
    if type(command) != str:  # like point x, y, heading
        # ITHQ controller doesn't handles this
        pass
    else:
        if command == 'dock':
            action_goto.command = command
            pass
        else:
            spaces_count = 0
            for char in command:
                if char == ' ':
                    spaces_count += 1
            if spaces_count == 3:
                action_goto.command = 'gotoPoint'
                action_goto.goal = command
            else:
                action_goto.command = 'goto'
                action_goto.goal = command
    return action_goto


def create_speak_action(command):
    speak_action = soundActionGoal()
    speak_action.type = 'voice'
    speak_action.info = command
    return speak_action


def wait_for_sub_to_publisher(publisher: rospy.Publisher, timeout=0):
    index = 0
    while publisher.get_num_connections() <= 0:
        time.sleep(0.2)
        if timeout:
            index += 1
            if index >= timeout*5:
                return False

    return True


if __name__ == "__main__":
    try:
        rospy.init_node(os.path.splitext(os.path.basename(__file__))[0])
        node = DigifabController()
        rospy.on_shutdown(node.node_shutdown)
        node.begin()
        rospy.spin()
        # signal.signal(signal.SIGINT, node.signal_handler)
    except Exception:
        rospy.logerr(traceback.format_exc())

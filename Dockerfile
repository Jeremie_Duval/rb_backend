FROM ros:noetic-ros-base

# Setup catkin workspace
ENV CATKIN_WS=/root/sirSteward_ws
RUN mkdir -p $CATKIN_WS/src
WORKDIR $CATKIN_WS/src
COPY . .
COPY ros_start_script.sh /
RUN chmod +x /ros_start_script.sh

WORKDIR $CATKIN_WS
RUN /bin/bash -c '. /opt/ros/noetic/setup.bash; catkin_make'

ENTRYPOINT ["/ros_start_script.sh"]
CMD ["bash"]

#FROM python:3.7
#ENV CATKIN_WS=/root/sirSteward_ws
#WORKDIR $CATKIN_WS/src
#COPY . .
#COPY requirements.txt /
#RUN chmod +x /requirements.txt
#RUN pip install -r requirements.txt


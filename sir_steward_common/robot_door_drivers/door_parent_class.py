import serial
from enum import Enum
from abc import ABC, abstractmethod


class DoorStatus(Enum):
    OPEN = 1
    OPENING = 2
    CLOSE = 3
    CLOSING = 4
    STANDBY = 5
    CALIBRATING = 6
    ERROR = 7
    PREEMPT = 8


class Door:
    def __init__(self, id):
        self.id = id
        self.state = DoorStatus.CLOSE


class DoorsClass(ABC):
    def __init__(self, num_doors):
        self.doors = [Door(index) for index in range(num_doors)]
        self.door_connexion: serial.Serial = None
        self.data = ""

    @abstractmethod
    def connect(self):
        pass

    @abstractmethod
    def open_door(self, num):
        pass

    @abstractmethod
    def close_door(self, num):
        pass

    @abstractmethod
    def stop_door_motion(self, num):
        pass

    @abstractmethod
    def calibrate_door(self):
        pass

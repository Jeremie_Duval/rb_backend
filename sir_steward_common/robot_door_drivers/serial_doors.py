#!/usr/bin/env python
import datetime
import time

import rospy
import serial
import threading
import traceback

from .door_parent_class import DoorsClass, Door, DoorStatus

# Commandes en Hexa :
# 0xAX pour ouvrir la porte X
# 0xBX pour fermer la porte X
# 0xCX permet l'arrêt des portes
# 0xD0 pour starter une calibration automatique des 3 portes
# 0xD1 0xXX 0xYY 0xZZ permet de set le torque d'erreur des moteurs

READ_TIMEOUT = 1
MAX_NUM_DOORS = 6
MAX_DOOR_MOTION_TIME = 15
SERIAL_PORT = '/dev/serial/by-id/usb-FTDI_FT230X_Basic_UART_D308B55J-if00-port0'

# These are translate Hex Value programmed in the PCB
DOOR_OPENED_INT = 0
DOOR_CLOSED_INT = 192
DOOR_ERROR_INT = 224

OPEN_DOOR_INT = 160
CLOSE_DOOR_INT = 176
PAUSE_DOOR_INT = 192

CALIBRATE_DOORS = 208
SET_TORQUE = 209


class SerialDoors(DoorsClass):
    def __init__(self, num_doors, serial_port):
        if num_doors < MAX_NUM_DOORS:
            super().__init__(num_doors)
            self.port = serial_port
            self.read_thread = threading.Thread(target=self.__listen)
            self.__write_serial_lock = threading.Lock()
            self.NUM_DOORS = num_doors
        else:
            raise ValueError(f"Number of doors should be equal or less than {MAX_NUM_DOORS}")

    def connect(self):
        try:
            self.door_connexion = serial.Serial(SERIAL_PORT, timeout=READ_TIMEOUT)  # open serial port
            if self.door_connexion.isOpen():
                self.read_thread.start()
                return True
            else:
                raise IOError("Serial connexion seemed to failed")
                return False
        except Exception:
            rospy.logerr(f'----- [SERIAL_DOOR] ***** {traceback.format_exc()} ***** -----')

    def write_to_serial(self, command):
        self.__write_serial_lock.acquire()
        if isinstance(command, int):
            self.door_connexion.write(bytes([command]))
        else:
            for byte in command:
                self.door_connexion.write(bytes([byte]))
                time.sleep(0.1)

        time.sleep(0.1)
        self.__write_serial_lock.release()

    def open_door(self, num):
        data = []
        if isinstance(num, int):
            data = OPEN_DOOR_INT + num
            self.doors[num].state = DoorStatus.OPENING
        else:
            for door_num in num:
                data.insert(len(data), OPEN_DOOR_INT + door_num)
                self.doors[door_num].state = DoorStatus.OPENING

        self.write_to_serial(data)
        rospy.loginfo(f"----- [SERIAL_DOOR] ***** Opening door {str(num)} ***** -----")

        return self.__wait_for_change_of_state(num, DoorStatus.OPENING)

    def close_door(self, num):
        data = []
        if isinstance(num, int):
            data = CLOSE_DOOR_INT + num
            self.doors[num].state = DoorStatus.CLOSING
        else:
            for door_num in num:
                data.insert(len(data), CLOSE_DOOR_INT + door_num)
                self.doors[door_num].state = DoorStatus.CLOSING

        self.write_to_serial(data)
        rospy.loginfo(f"----- [SERIAL_DOOR] ***** Closing door {str(num)} ***** -----")

        return self.__wait_for_change_of_state(num, DoorStatus.CLOSING)

    def stop_door_motion(self):
        num = 0
        rospy.loginfo("----- [SERIAL_DOOR] ***** Stopping door motion ***** -----")
        for door in self.doors:
            if door.state in [DoorStatus.OPENING, DoorStatus.CLOSING, DoorStatus.PREEMPT]:
                self.write_to_serial(PAUSE_DOOR_INT + num)
                self.doors[num].state = DoorStatus.STANDBY
            num += 1
        return 'All doors have been stopped'

    def calibrate_door(self):
        self.write_to_serial(CALIBRATE_DOORS)
        for door in self.doors:
            door.state = DoorStatus.CALIBRATING
        rospy.loginfo("----- [CALIBRATION] ***** Calibrating doors ***** -----")
        return self.__wait_for_change_of_state(list(range(0, self.NUM_DOORS)), DoorStatus.CALIBRATING)

    def set_torque(self, torque):
        data = [SET_TORQUE]
        for door in range(0, self.NUM_DOORS):
            data.insert(len(data), torque)
        self.write_to_serial(data)
        rospy.loginfo(f"----- [SERIAL_DOOR] ***** Maximum torque on all {self.NUM_DOORS} doors "
                      f"is now {torque/100}% ***** -----")

    def __wait_for_change_of_state(self, door_num, state):
        timermotion = datetime.datetime.now()
        while not rospy.is_shutdown() or datetime.datetime.now()-timermotion < MAX_DOOR_MOTION_TIME:
            if isinstance(door_num, list) \
                    and all(self.doors[door_num[num]].state is not state for num in range(len(door_num))):
                return f'Doors {door_num} are now {self.doors[door_num[0]].state}'
            if isinstance(door_num, int) and self.doors[door_num].state is not state:
                return f'Door {self.doors[door_num].id} is now {self.doors[door_num].state}'
            time.sleep(0.5)
        return "ERROR: NO CHANGE OF STATE"

    def __listen(self):
        while not rospy.is_shutdown():
            # TODO: Test entre timeout et recevoir porte 0 ouverte
            self.data = self.door_connexion.read()  # should return 0 if timeout_reach
            if self.data != b'':
                self.data = ord(self.data)
                door_event = self.data & int('0xF0', 16)  # Filter first nible
                door_num = self.data & int('0x0F', 16)  # Filter second nible
                if door_event == DOOR_OPENED_INT:
                    if self.data == int('0x06', 16):  # aknowledge
                        pass
                    else:
                        self.doors[self.data].state = DoorStatus.OPEN

                elif door_event == DOOR_CLOSED_INT:
                    self.doors[door_num].state = DoorStatus.CLOSE

                elif door_event == DOOR_ERROR_INT:
                    self.doors[door_num].state = DoorStatus.ERROR

    def door_action(self, command, door_list):
        if 'open' in command:
            return self.open_door(door_list)
        elif 'close' in command:
            return self.close_door(door_list)

    def set_state_to_preempt(self):
        for door in self.doors:
            door.state = DoorStatus.PREEMPT


if __name__ == '__main__':
    door_controller = SerialDoors(3, serial_port='/dev/serial/by-id/usb-FTDI_FT230X_Basic_UART_D308B55J-if00-port0')
    door_controller.connect()
    door_controller.calibrate_door()
    door_controller.set_torque(125)
    door_controller.open_door(1)
    print(f'Change of command')
    door_controller.close_door(1)




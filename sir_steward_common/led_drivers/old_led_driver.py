# This is a sample Python script.

# Press Maj+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import time
import serial
import threading


# Dictionary representing the morse code chart
MORSE_CODE_DICT = {'A': '.-', 'B': '-...',
                   'C': '-.-.', 'D': '-..', 'E': '.',
                   'F': '..-.', 'G': '--.', 'H': '....',
                   'I': '..', 'J': '.---', 'K': '-.-',
                   'L': '.-..', 'M': '--', 'N': '-.',
                   'O': '---', 'P': '.--.', 'Q': '--.-',
                   'R': '.-.', 'S': '...', 'T': '-',
                   'U': '..-', 'V': '...-', 'W': '.--',
                   'X': '-..-', 'Y': '-.--', 'Z': '--..',
                   '1': '.----', '2': '..---', '3': '...--',
                   '4': '....-', '5': '.....', '6': '-....',
                   '7': '--...', '8': '---..', '9': '----.',
                   '0': '-----', ', ': '--..--', '.': '.-.-.-',
                   '?': '..--..', '/': '-..-.', '-': '-....-',
                   '(': '-.--.', ')': '-.--.-'}

COLOR_DICT = {'off': '0', 'red': '1', 'green': '2', 'yellow': '3', 'blue': '4', 'purple': '5', 'cyan': '6',
              'white': '7',
              'o': '0', 'r': '1', 'g': '2', 'y': '3', 'b': '4', 'p': '5', 'c': '6', 'w': '7',
              '0': '0', '1': '1', '2': '2', '3': '3', '4': '4', '5': '5', '6': '6', '7': '7'}


class LedController:
    """
       This class provides a control on the color of the LED, the state between solid or blinking and the rate
       of the blinking
    """

    def __init__(self, port='COM7', color='off', blink=False, period_ms=500):  # Utiliser un dict. pour les couleurs et code

        # initialising the 3 control variable
        self.color = color
        self.blink = blink
        self.period_ms = period_ms

        # handling thread
        self.thread = None
        self.stopThread = False

        # initialising the connection to the port
        # TODO: create a function to find the port dynamically
        self.__connect_serial(port=port)

        self.set_multiple_value(self.color, self.blink, self.period_ms)

    @staticmethod
    def __convert_blink_state_to_command(blink):
        blink_command = b'B' if blink else b'S'
        return blink_command

    @staticmethod
    def __convert_period_to_command(period_byte):
        period_command = bytes([ord('T'), period_byte])
        return period_command

    def __connect_serial(self, port='COM7', baud_rate=9600):
        self.ser = serial.Serial(port, baud_rate)
        if self.ser.isOpen():
            print("connection Done")

    def __start_threading(self, function):
        self.__stop_threading()
        self.thread = threading.Thread(target=function)
        self.thread.start()

    def __stop_threading(self):
        if self.thread is not None:
            self.stopThread = True
            self.thread.join()
            self.thread = None
            self.stopThread = False

    def write_to_serial(self, command):
        self.ser.write(command)

    def reset(self):
        self.write_to_serial(b'R')

    def turn_off(self):
        self.reset()
        self.set_color('off')

    def set_color(self, color):
        self.__stop_threading()
        if COLOR_DICT.get(color.lower()):
            self.color = color.lower()
            color_command = COLOR_DICT[self.color].encode()
            self.write_to_serial(color_command)
        else:
            raise ValueError('Color not compatible for this LED type. It sould be one of these: red, '
                             'green, blue, yellow, purple, cyan, white or off')

    def set_blinking_state(self, blink):
        self.__stop_threading()
        self.blink = blink
        blink_command = self.__convert_blink_state_to_command(blink)
        self.write_to_serial(blink_command)

    def set_period(self, period_ms):
        self.__stop_threading()
        period_byte = int(period_ms / 100)
        if 1 <= period_byte <= 255:
            self.period_ms = period_ms
            period_byte_command = self.__convert_period_to_command(period_byte)
            self.write_to_serial(period_byte_command)
        else:
            raise ValueError('Period should be between 100 and 25 000 ms')

    def set_multiple_value(self, color=None, blink=None, period_ms=None):
        if color is not None:
            self.set_color(color)
        if period_ms is not None:
            self.set_period(period_ms)
        if blink is not None:
            self.set_blinking_state(blink)

    def get_color(self):
        return self.color

    def get_blink_state(self):
        return self.blink

    def get_period(self):
        return self.period_ms

    def get_all_value(self):
        return self.color, self.blink, self.period_ms

    def do_rainbow(self):
        self.__start_threading(self.__do_rainbow_thread)

    def _easter_egg(self):
        self.__start_threading(self.__send_morse_message_thread)

    def __do_rainbow_thread(self):
        self.write_to_serial(b'S')
        color = 1
        while not self.stopThread:
            color = color % 8
            self.write_to_serial(str(color).encode())
            color += 1
            time.sleep(1)
        print("rainbow done")

    def __send_morse_message_thread(self, text="Welcome from Sir Steward"):
        morse = self.__convert_text_to_morse(text)
        self.__convert_morse_to_led(morse)

    @staticmethod
    def __convert_text_to_morse(text):
        # Function to encrypt the string
        # according to the morse code chart
        text = text.upper()
        morse = ''
        for letter in text:
            if letter != ' ':

                morse += MORSE_CODE_DICT[letter] + ' '
            else:
                morse += ' '

        return morse

    def __convert_morse_to_led(self, morse):
        sleep = 0.0
        for symbol in morse:
            if self.stopThread:
                break
            if symbol == '.':
                sleep = 0.3
            if symbol == '-':
                sleep = 1

            self.write_to_serial(b'7')
            time.sleep(sleep / 2)
            self.write_to_serial(b'0')
            time.sleep(0.6 / 2)


if __name__ == '__main__':
    led_controller = LedController('COM13', '1', False, 1000)
    time.sleep(3)
    led_controller.set_multiple_value('purple', True, 500)
    time.sleep(3)
    led_controller.turn_off()
    time.sleep(3)
    led_controller.set_multiple_value('purple', True, 500)



#!/usr/bin/env python

import time
import serial
import threading

import rospy
from .led_parent_class import LedClass

# Commandes en Hexa :
# 0xAX pour allumer le channel X (0~2)
# 0xBX pour éteindre le channel X (0~2)
# 0xD0 pour le mode "rainbow"
# 0xD1 pour le mode "solid"
# 0xDA 0xRR 0xGG 0xBB pour envoyer les 8bits d'intensité de chaque couleur

# Le changement des couleurs/modes/channels allumés s'écrit dans la flash et la config est récupérée au redémarrage du
# module

COLOR_DICT = {'on': 160, 'off': 176, 'solid': 209, 'rainbow': 208, 'white': [218, 0, 0, 0],
              'red': [218, 0, 0, 255], 'rose': [218, 128, 0, 255], 'magenta': [218, 255, 0, 255],
              'violet': [218, 255, 0, 128], 'blue': [218, 255, 0, 0], 'azure': [218, 255, 128, 0],
              'cyan': [218, 255, 255, 0], 'spring_green': [218, 128, 255, 0], 'green': [218, 0, 255, 0],
              'chartreuse': [218, 0, 255, 128], 'yellow': [218, 0, 255, 255], 'orange': [218, 0, 128, 255]}


class LedRGBPWM(LedClass):
    def __init__(self, baud_rate=9600, port='COM7', color='off', blink=False, period_ms=500):
        super().__init__(port, color, blink, period_ms)
        self.baud_rate = baud_rate
        self.__write_serial_lock = threading.Lock()

    def write_to_serial(self, command):
        self.__write_serial_lock.acquire()
        if not command == COLOR_DICT['rainbow']:
            self.led_connexion.write(bytes([COLOR_DICT['solid']]))
            time.sleep(0.1)
        if isinstance(command, int):
            self.led_connexion.write(bytes([command]))
        else:
            for byte in command:
                self.led_connexion.write(bytes([byte]))
                time.sleep(0.1)

        time.sleep(0.1)
        self.__write_serial_lock.release()

    def connect(self):
        self.led_connexion = serial.Serial(self.port, self.baud_rate)
        # self.led_connexion.read_until()
        if self.led_connexion.isOpen():
            rospy.loginfo('----- [LED_RGB_PWM] ***** Led RGB PWM is ready ***** -----')
            self.turn_on()
            return True
        else:
            rospy.logwarn('----- [LED_RGB_PWM] ***** Led RGB PWM could not connect ***** -----')
            return False

    def turn_off(self):
        # TODO: Turn off only selected channel
        self.set_color('white')
        for channel in range(0, 3):
            self.write_to_serial(COLOR_DICT['off'] + channel)

    def turn_on(self):
        # TODO: Turn on only selected channel
        for channel in range(0, 3):
            self.write_to_serial(COLOR_DICT['on'] + channel)
        self.set_color('white')

    def set_rainbow(self):
        self.write_to_serial(COLOR_DICT['rainbow'])

    def set_solid(self):
        self.write_to_serial(COLOR_DICT['solid'])

    def set_color(self, color: str, offset=0):
        color = color.lower()
        if COLOR_DICT.get(color):
            color_command = COLOR_DICT[color]
            self.write_to_serial(color_command)
        else:
            raise ValueError('Color not compatible for this LED type. It should be one of these: white, red, rose, '
                             'magenta, violet, blue, azure, cyan, spring_green, green, chartreuse, yellow or red')

    def set_rgb_color(self, r=0, g=0, b=0):
        colors = [r, g, b]
        error_in_colors = False
        if all(isinstance(color, int) for color in colors):
            for element in colors:
                if 255 <= element <= 0:
                    error_in_colors = True
            if not error_in_colors:
                self.write_to_serial([218, b, g, r])
        else:
            error_in_colors = True

        if error_in_colors:
            rospy.logwarn(f'----- [LED_RGB_PWM] ***** R, G and B must be an integer between 0 and 255 ***** -----')

    def reset(self):
        pass

    def set_blinking_state(self, blink):
        pass

    def set_period(self, period_ms):
        pass

    def set_multiple_value(self, color=None, blink=None, period_ms=None):
        pass


if __name__ == '__main__':
    led_controller = LedRGBPWM(port='/dev/serial/by-id/usb-FTDI_FT230X_Basic_UART_DN05XJJM-if00-port0', channel=2)
    led_controller.connect()
    time.sleep(1)
    led_controller.set_solid()
    time.sleep(1)
    led_controller.set_color('cyan')
    time.sleep(5)
    led_controller.set_rgb_color(255, 0, 0)
    time.sleep(5)
    led_controller.turn_off()

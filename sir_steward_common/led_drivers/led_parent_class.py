# This is a sample Python script.

# Press Maj+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
from abc import ABC, abstractmethod


class LedClass(ABC):
    """
       This class provides a control on the color of the LED, the state between solid or blinking and the rate
       of the blinking
    """

    def __init__(self, port='COM7', color='off', blink=False,
                 period_ms=500):  # Utiliser un dict. pour les couleurs et code

        # initialising the 3 control variable
        self.color = color
        self.blink = blink
        self.period_ms = period_ms
        self.port = port

        self.led_connexion = None

    @abstractmethod
    def connect(self):
        pass

    @abstractmethod
    def reset(self):
        pass

    @abstractmethod
    def turn_off(self):
        pass

    @abstractmethod
    def set_color(self, color):
        pass

    @abstractmethod
    def set_rgb_color(self, r=0, g=0, b=0):
        pass

    @abstractmethod
    def set_blinking_state(self, blink):
        pass

    @abstractmethod
    def set_period(self, period_ms):
        pass

    @abstractmethod
    def set_multiple_value(self, color=None, blink=None, period_ms=None):
        pass


#!/usr/bin/env python

import time
import serial
import threading
import rospy
from .led_parent_class import LedClass

COLOR_DICT = {'off': '0', 'red': '1', 'green': '2', 'yellow': '3', 'blue': '4', 'purple': '5', 'cyan': '6',
              'white': '7',
              'o': '0', 'r': '1', 'g': '2', 'y': '3', 'b': '4', 'p': '5', 'c': '6', 'w': '7',
              '0': '0', '1': '1', '2': '2', '3': '3', '4': '4', '5': '5', '6': '6', '7': '7'}


class Led7Colors(LedClass):
    def __init__(self, baud_rate=9600, port='COM7', color='off', blink=False, period_ms=500):
        super().__init__(port, color, blink, period_ms)
        self.baud_rate = baud_rate
        self.__write_serial_lock = threading.Lock()

    def write_to_serial(self, command):
        self.__write_serial_lock.acquire()
        self.led_connexion.write(command)
        time.sleep(0.1)
        self.__write_serial_lock.release()

    def connect(self):
        self.led_connexion = serial.Serial(self.port, self.baud_rate)
        self.led_connexion.read_until()
        if self.led_connexion.isOpen():
            rospy.loginfo('----- [LED7COLORS] ***** led7Colors is ready ***** -----')
            return True
        else:
            rospy.logwarn(f'----- [LED7COLORS] ***** led7Colors could not connect ***** -----')
            return False

    def reset(self):
        self.write_to_serial(b'R')

    def turn_off(self):
        self.reset()
        self.set_color('off')

    def set_color(self, color: str):
        color = color.lower()
        if COLOR_DICT.get(color):
            self.color = color
            color_command = COLOR_DICT[self.color].encode()
            self.write_to_serial(color_command)
        else:
            raise ValueError('Color not compatible for this LED type. It should be one of these: red, '
                             'green, blue, yellow, purple, cyan, white or off')

    def set_blinking_state(self, blink: bool):
        self.blink = blink
        blink_command = self.__convert_blink_state_to_command(blink)
        self.write_to_serial(blink_command)

    def set_period(self, period_ms: int):
        period_byte = int(period_ms / 100)
        if 1 <= period_byte <= 255:
            self.period_ms = period_ms
            period_byte_command = self.__convert_period_to_command(period_byte)
            self.write_to_serial(period_byte_command)
        else:
            raise ValueError('Period should be between 100 and 25 000 ms')

    def set_multiple_value(self, color=None, blink=None, period_ms=None):
        if color is not None:
            self.set_color(color)
        if blink is not None:
            self.set_blinking_state(blink)
        if period_ms not in [None, 0]:
            self.set_period(period_ms)

    def set_rgb_color(self, r=0, g=0, b=0):
        pass

    @staticmethod
    def __convert_blink_state_to_command(blink):
        blink_command = b'B' if blink else b'S'
        return blink_command

    @staticmethod
    def __convert_period_to_command(period_byte):
        period_command = bytes([ord('T'), period_byte])
        return period_command


if __name__ == '__main__':
    led_controller = Led7Colors('COM13', '1', False, 1000)
    time.sleep(3)
    led_controller.set_multiple_value('purple', True, 500)
    time.sleep(3)
    led_controller.turn_off()
    time.sleep(3)
    led_controller.set_multiple_value('purple', True, 500)

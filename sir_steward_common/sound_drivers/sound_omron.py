"""THIS CLASS IS UNUSUAL FROM THE REST
 BECAUSE IT USES services and actions runed by other nodes
It is then depending from them. It is the only way for the moment to create sound from the robot"""

# !/usr/bin/env python
import rospy
import actionlib
from time import sleep
from .sound_parent_class import soundClass
from omron_base_nodes.srv import arclSrv, arclSrvRequest, arclSrvResponse


class OmronSound(soundClass):
    def __init__(self, port='COM7'):
        super().__init__(port)
        self.omron_service = rospy.ServiceProxy('arcl_services', arclSrv)

    def connect(self):
        rospy.loginfo("----- [SOUND_DRIVER] ***** Already connected to omron base ***** ------")
        return True

    def play(self, sound_info):
        sound_command = ''
        if sound_info.get('type') == 'file':
            sound_command = 'play'
        if sound_info.get('type') == 'voice':
            sound_command = 'say'
        sound_command = f"{sound_command} {sound_info.get('info')}"
        self.omron_service.call(sound_command)
        self.__timer(sound_info.get('time'))

    def pause(self):
        rospy.loginfo("----- [SOUND_DRIVER] *****  This hardware can not perform 'pause' command ***** ------")
        pass

    def stop(self):
        rospy.loginfo("----- [SOUND_DRIVER] *****  This hardware can not perform 'stop' command ***** ------")
        pass

    @staticmethod
    def __timer(timer):
        timer_counter = timer if timer > 0 else 0
        while not rospy.is_shutdown() and timer_counter > 0:
            sleep(1)
            timer_counter -= 1

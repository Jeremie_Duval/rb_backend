# This is a sample Python script.

# Press Maj+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
from abc import ABC, abstractmethod


class soundClass(ABC):
    """
       This class provides a control on the color of the LED, the state between solid or blinking and the rate
       of the blinking
    """

    def __init__(self, port='COM7'):  # Utiliser un dict. pour les couleurs et code

        self.port = port


    @abstractmethod
    def connect(self):
        pass

    @abstractmethod
    def play(self, sound_info):
        pass

    @abstractmethod
    def pause(self):
        pass

    @abstractmethod
    def stop(self):
        pass

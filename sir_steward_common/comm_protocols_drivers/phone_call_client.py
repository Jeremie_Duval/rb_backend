import vonage
import os
import rospy

dir = os.path.abspath(os.path.join(__file__, "../../.."))
VONAGE_APPLICATION_PRIVATE_KEY_PATH = dir + "/sir_steward_common/comm_protocols_drivers/Private_key/private.key"
VONAGE_APPLICATION_ID = "494cf900-809e-4cce-b01c-5e77ae97bc6d"
LANGUAGE = {
    'french': 'fr-CA',
    'english': 'en-US'
}


class PhoneCallClient:
    def __init__(self):
        self.client = vonage.Client(application_id=VONAGE_APPLICATION_ID,
                                    private_key=VONAGE_APPLICATION_PRIVATE_KEY_PATH)

    def voice_call(self, phone_number='', text='', language=LANGUAGE['french'], language_style=2):
        voice = vonage.Voice(self.client)

        text = text.replace('PAUSE', "<break time='1s' />")

        response = voice.create_call({
            'to': [{'type': 'phone', 'number': phone_number}],
            'from': {'type': 'phone', 'number': "18194713426"},
            'ncco': [{'action': 'talk', 'text': "<speak><break time='2s' />"+text+"</speak>",
                      'language': language, 'style': language_style}]
        })

        if response:
            rospy.loginfo(f'----- [PHONE] ***** Phone call sent successfully ***** -----')
            return "Phone call sent successfully."
        else:
            rospy.logwarn(f"----- [PHONE] ***** Phone call didn't send correctly ***** -----")
            return f"Phone call didn't send correctly"


if __name__ == '__main__':
    phone = PhoneCallClient()
    phone.voice_call('14505024001', 'Test pour voir si la langue est correcte')



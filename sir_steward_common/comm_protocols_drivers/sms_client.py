import vonage
import rospy


class SmsClient:
    def __init__(self):
        self.client = vonage.Client(key="9fa394b8", secret="tJ1zM905qsDqxgkT")
        self.sms = vonage.Sms(self.client)

    def send_msg(self, phone_number='', text=''):

        response_data = self.sms.send_message(
            {
                "from": "12262416188",
                "to": phone_number,
                "text": text,
            }
        )

        if response_data["messages"][0]["status"] == "0":
            rospy.loginfo(f"----- [PHONE] ***** Message sent successfully ***** -----")
            return "Message sent successfully"
        else:
            rospy.logwarn(f"----- [PHONE] ***** Message failed with error: "
                          f"{response_data['messages'][0]['error-text']} ***** -----")
            return f"Message failed with error: {response_data['messages'][0]['error-text']}"



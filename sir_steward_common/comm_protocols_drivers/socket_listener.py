import socket
import time
from collections import defaultdict
import rospy

RECV_BUFFER = 4096

"""
A driver class to handle all input and output communication with ARCL server.
"""


class SocketListener(object):
    def __init__(self, ip_address, port):
        self.sock = None
        self.actual_address = None
        self.addr = str(ip_address)
        self.port = port
        self._closing_connection = False

        self.responses = defaultdict(list)

    def connect(self):
        while True:  # allow retrying connexion when problem occurs
            if self._closing_connection:
                self._closing_connection = False
                return False
            try:
                sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                connexion = (self.addr, self.port)
                sock.bind(connexion)
                rospy.loginfo(f"----- [SOCKET] ***** Socket connexion successfully establish with "
                              f"{self.__class__.__name__} ***** -----")
            except socket.error as e:
                rospy.logwarn(f"----- [SOCKET] *****A problem occurs with {self.__class__.__name__} "
                              f"--- Retrying to connect ***** -----")
                pass
            else:
                break
            finally:
                time.sleep(1)
        sock.listen(1)
        sock_connect, addr_and_port = sock.accept()
        sock.shutdown(socket.SHUT_RDWR)
        sock.close()  # Close the initial socket.
        self.actual_address = addr_and_port
        self.sock = sock_connect
        rospy.loginfo(f"----- [SOCKET] ***** Connected with {addr_and_port} ***** -----")
        return True

    def get_sorted_data(self):
        data = self.sock.recv(RECV_BUFFER)
        self.responses = defaultdict(list)
        if not data:
            rospy.logerr("----- [SOCKET] ***** Socket connexion has been lost, trying to reconnect ***** -----")
            self.reconnect()
            return False
        self.__sort_data(data)
        return self.responses

    def close(self):
        rospy.loginfo(f"----- [SOCKET] ***** Closing socket of {self.actual_address} ***** -----")
        self._closing_connection = True
        self.sock.shutdown(socket.SHUT_RDWR)
        self.sock.close()

    def reconnect(self):
        self.close()
        self._closing_connection = False
        self.sock = None
        self.connect()

    def __sort_data(self, data):
        for line in data.splitlines():
            tuple = line.decode('utf8').split(":")
            key = tuple[0]
            value = tuple[1] if 1 < len(tuple) else ""
            self.responses[key] = value


if __name__ == "__main__":
    # ip_address = "192.168.50.109"
    ip_address = "192.168.3.1"
    port = 7179
    socket_listener = SocketListener(ip_address, port)
    socket_listener.connect()
    for i in range(0, 5):
        dict_response = socket_listener.get_sorted_data()
        print(dict_response)
    socket_listener.close()

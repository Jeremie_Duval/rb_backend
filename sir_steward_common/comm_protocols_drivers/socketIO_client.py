import traceback

import rospy
import socketio
from aiohttp import web

sio = socketio.AsyncServer(cors_allowed_origins='*')


class SocketIO:
    def __init__(self, port=8000):
        self.app = None
        self.port = port

    def connect(self):
        try:
            web.run_app(self.__connect(), port=self.port)
        except Exception:
            rospy.logerr(f'----- [SOCKET_IO] ***** {traceback.format_exc()} ***** -----')

    async def __connect(self):
        # Start web server
        self.app = web.Application()
        sio.attach(self.app)
        rospy.loginfo("----- [SOCKET_IO] ***** Web_app running ***** -----")
        return self.app

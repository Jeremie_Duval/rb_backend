import threading
import telnetlib
import sys
import signal
import time
from datetime import datetime

import rospy

RECONNECTION_TIME = 2
# IP_ADDRESS = "192.168.50.109"
IP_ADDRESS = "192.168.3.1"
# IP_ADDRESS = "192.168.50.75"
PORT = "7171"
PASSWD = "Amvic145"


class TelnetOmron:
    """
        Class for Omron LD robot integration.
        """

    def __init__(self, telnet_host, telnet_port, telnet_password):
        # Private property
        self._host = telnet_host
        self._port = telnet_port
        self._password = telnet_password
        self._tn = None
        self._closing_connection = False
        self._conn_thread = threading.Thread(target=self.__connection_thread)

        # Public property
        self.conn_ok = False
        self.response = None
        self.is_alive = False

    def close(self):
        self._closing_connection = True
        if self.conn_ok:
            try:
                self._conn_thread.join()
                self._tn.close()
            except Exception:
                #Todo: Maybe find a better way to close connection
                sys.exit("Closing Telnet in middle of a connection")
                pass
        self.is_alive = False

    def connect(self):
        if self.__connect():
            if not self._conn_thread.is_alive():
                self._conn_thread.start()
            return True
        else:
            return False

    def read_line(self):
        return self._tn.read_until(b"\n", timeout=2).decode("utf8").lower()

    def send(self, command):
        self.__send(command)

    def is_alive(self):
        return self.is_alive

    def __connection_thread(self):
        while not self._closing_connection:
            if self.conn_ok:
                while not self._closing_connection:
                    if self.__check_connection():
                        time.sleep(1)
                    else:
                        if not self.__connect():
                            self.is_alive = False
                            rospy.loginfo(f"----- [TELNET] ***** No connexion anymore ***** -----")
            else:
                rospy.loginfo(f"----- [TELNET] ***** An unrecoverable error occurred ***** -----")
                return

    def __connect(self):
        recon_index = 0
        while True:
            try:
                if self._closing_connection:
                    self._closing_connection = False
                    return False
                telnet_conn = telnetlib.Telnet(self._host, self._port, timeout=5)
            except KeyboardInterrupt:
                return False
            except Exception:
                rospy.loginfo(f"----- [TELNET] ***** Impossible to connect to host: {self._host} ***** -----")
                if recon_index > 5:
                    rospy.loginfo(f"----- [TELNET] ***** Reconnection limit has been reach, stop retrying ***** -----")
                    self.conn_ok = False
                    return self.conn_ok
                elif not self._closing_connection:
                    rospy.loginfo(f"----- [TELNET] ***** Try reconnecting in {RECONNECTION_TIME}s... ***** -----")
                    time.sleep(RECONNECTION_TIME)
                    recon_index += 1
            else:
                rospy.loginfo(f"----- [TELNET] ***** Connection established ***** -----")
                self._tn = telnet_conn
                break

        self._tn.read_until(b"Enter password:", timeout=2)
        if self.__send_until_answer(self._password, expect="Welcome to the server"):
            rospy.loginfo(f"----- [TELNET] ***** Ready to send commands ***** -----")
            self._tn.read_until(b"End of commands", timeout=2)
            self.conn_ok = True
            self.is_alive = True
        else:
            rospy.loginfo(f"----- [TELNET] ***** Wrong password ***** -----")
            self.conn_ok = False
        return self.conn_ok

    def __check_connection(self):
        try:
            self._tn.read_very_eager()
        except EOFError:
            rospy.loginfo(f"----- [TELNET] ***** Connection is closed ***** -----")
            self.conn_ok = False
        finally:
            return self.conn_ok

    def __send(self, command):
        """Send MX telnet command"""
        if not command.endswith("\n"):
            command = command + "\n"
        self._tn.write(command.encode("utf8"))

    def __send_until_answer(self, command, expect):
        if isinstance(expect, str):
            expect = [expect]
        self.response = b""
        self.__send(command)
        encode_expect = [x.encode("utf8") for x in expect]
        try:
            # self.response = self._tn.read_until(expect, timeout=5)
            self.response = self._tn.expect(encode_expect, 30)
            if self.response[1] is None:
                raise ValueError
            # self.response[2].decode()
        except Exception as e:
            rospy.loginfo(f"----- [TELNET] ***** No expected answer received. Error: {e} ***** -----")
            return False
        else:
            return True


if __name__ == '__main__':
    rb = TelnetOmron(IP_ADDRESS, PORT, PASSWD)
    rb.connect()
    rb1 = TelnetOmron(IP_ADDRESS, PORT, PASSWD)
    rb1.connect()
    time.sleep(3)
    rb.send("Goto test1")
    time.sleep(4)
    rb1.send_and_confirm("stop","Stopped")
    hola =rb.send_and_receive("faultsGet", "End of FaultList")
    print(hola)
    time.sleep(1)
    answer = rb.get_command_answer("getConfigSectionInfo")
    print(answer)
    rb.send("getConfigSectionInfo")
    print(rb.read_until(answer))

#!/usr/bin/env python3.8

import threading
import os
import time
import traceback
from multiprocessing import Process
import rosgraph
import rospy
import asyncio
import rosnode
import actionlib
from aiohttp import web
from std_msgs.msg import String
from ui_client.ui_parent_class import UiUserClass
from ui_user_nodes.msg import pantheraGoalsList, pantheraGoals, travellingMsg
from ui_user_nodes.msg import timerActionAction, timerActionGoal, timerActionResult
from ui_user_nodes.msg import timerActionFeedback

from comm_protocols_drivers.socketIO_client import sio, SocketIO

DELIVERY_GOAL_PUB = rospy.get_param('panthera_commands', 'panthera_delivery_goal')
DELIVERY_GOAL_LIST_SUB = rospy.get_param('panthera_list_goals', 'panth_list_goals')
TRAVELLING_COMMANDS = rospy.get_param('digifab_travelling_messages', 'digifab_text')
COUNTER = rospy.get_param('panthera_counter', 'panth_counter')
OMRON_BATTERY = rospy.get_param('omron_battery', 'omron_base_SoC')
PANTHERA_TIMER = rospy.get_param('panthera_timer_action', 'panthera_timer')


class DigifabUI(UiUserClass):

    def __init__(self, ip_address: str, port: int, gnrl_cmd_pub, status_pub):
        super().__init__(ip_address, port, gnrl_cmd_pub, status_pub)

        # Publisher
        self.delivery_goal_pub = rospy.Publisher(DELIVERY_GOAL_PUB, String, queue_size=10)
        # Subscriber
        self.traveling_message_sub = rospy.Subscriber(TRAVELLING_COMMANDS, travellingMsg,
                                                      self.__change_travelling_msg, queue_size=10)
        self.list_of_goals_sub = rospy.Subscriber(DELIVERY_GOAL_LIST_SUB, pantheraGoalsList,
                                                  self.__change_list_of_goals, queue_size=10)
        self.battery_sub = rospy.Subscriber(OMRON_BATTERY, String, self.__change_battery, queue_size=10)
        self.counter_sub = rospy.Subscriber(COUNTER, String, self.__change_counter, queue_size=10)

        # Action
        self.time_action = actionlib.SimpleActionServer(PANTHERA_TIMER, timerActionAction,
                                                        self.__timer_action_cb, auto_start=False)
        self.time_action.register_preempt_callback(self.__timer_interrupt_cb)

        # UI_variables
        self.battery_soc = {"state_of_charge": '--'}
        self.traveling_message = {"title": "Robot en initialisation", "text": "", "color": "gray"}
        self.list_of_goals = [{"id": 'restart', "name": 'restart'}]
        self.counter = 0

        # self.loop = asyncio.get_event_loop()
        self.sio_is_connected = False
        self.si = SocketIO(self.port)
        self.async_loop = None
        self.sid_list = []

        # Trouver un moyen de mettre a jour sans flag
        self.text_flag = False
        self.battery_flag = False

        # run_time flags
        self.is_playing = False

        # Since we use an other process to start the web server, we need to have a secondary node
        if self.wait_for_parent_node():
            rospy.init_node("digifab_ui_node")
        else:
            print("-------Parent node did not wake up ------------")
            raise ConnectionError

    def wait_for_parent_node(self):
        retry = 0
        print('Waiting for parent node to connect...')
        while retry < 5:
            # TODO: Do not hardcode the ui_user_node, sent it by creating pantherUI
            if rosnode.rosnode_ping('ui_user_node', 5):
                print('ui_parent_node is now wake up')
                return True
            retry += 1
            time.sleep(0.2)
        return False

    def connect_ui(self):
        self.time_action.start()
        asyncio.gather(self.__sio_callback(), self.__sio_emit_loop())
        self.si.connect()
        # sio.wait()

    def close(self):
        for sid in self.sid_list:
            self.async_loop.create_task(sio.disconnect(sid))
        self.async_loop.create_task(self.si.app.shutdown())
        self.async_loop.stop()
        self.async_loop.close()
        rospy.loginfo("closing connexion with panthera ui client")

    def __change_travelling_msg(self, trav_mess: travellingMsg):
        self.traveling_message['text'] = ''
        self.traveling_message['title'] = ''
        self.traveling_message['titlecolor'] = ''
        self.traveling_message['textcolor'] = ''
        if not trav_mess.clean:
            if trav_mess.text:
                self.traveling_message['text'] = trav_mess.text.encode('utf8').decode('utf8')
            if trav_mess.title:
                self.traveling_message['title'] = trav_mess.title.encode('utf8').decode('utf8')
            if trav_mess.titlecolor:
                self.traveling_message['titlecolor'] = trav_mess.titlecolor
            if trav_mess.textcolor:
                self.traveling_message['textcolor'] = trav_mess.textcolor

        self.text_flag = True

    def __change_list_of_goals(self, list: pantheraGoalsList):
        list_of_goals_dict = []
        for goal_element in list.list_of_goals:
            goal = {"id": goal_element.goal_id, "name": goal_element.goal_name}
            list_of_goals_dict.append(goal)

        self.list_of_goals = list_of_goals_dict

    def __change_counter(self, counter: String):
        self.counter = counter.data

    def __change_battery(self, battery: String):
        if self.battery_soc["state_of_charge"] != battery.data:
            self.battery_soc["state_of_charge"] = battery.data
            self.battery_flag = True

    def __timer_action_cb(self, timer: timerActionGoal):
        result = timerActionResult()
        # timer_counter because self.counter could be change by a string var
        timer_counter = 0
        if self.sio_is_connected:
            self.is_playing = True
            timer_counter = timer.time if timer.time > 0 else 0
            self.counter = timer_counter
            while not rospy.is_shutdown() and self.time_action.is_active() and timer_counter > 0:
                time.sleep(1)
                if timer.is_stoppable and not self.is_playing:
                    feedback = timerActionFeedback()
                    feedback.feedback = 'Timer has been paused'
                    self.time_action.publish_feedback(feedback)
                else:
                    timer_counter -= 1
                    self.counter = timer_counter

            if self.time_action.is_active():
                result.details = 'Timer done'
                self.time_action.set_succeeded(result)
                # rospy.loginfo("Timer is done")
        else:
            result.details = "Web server still not up"
            self.time_action.set_aborted(result)

    def __timer_interrupt_cb(self):
        rospy.loginfo('Stopping timer')
        result = timerActionResult()
        result.details = "Stopping timer"
        self.time_action.set_preempted(result)
        self.counter = 0

    # async def look_for_roscore(self):
    #     return rosgraph.is_master_online()

    async def __sio_emit_loop(self):
        battery_soc = dict(self.battery_soc)  # Or else it is passed by reference
        travelling_message = dict(self.traveling_message)
        list_of_goals = list(self.list_of_goals)
        counter = self.counter
        while not rospy.is_shutdown():
            if counter != self.counter:
                await sio.emit("patrolTimer", self.counter)
                counter = self.counter
            if list_of_goals != self.list_of_goals:
                await sio.emit("listGoals", self.list_of_goals)
                list_of_goals = self.list_of_goals
            if self.text_flag:
                await sio.emit("message", self.traveling_message)
                self.text_flag = False
            if self.battery_flag:
                await sio.emit("batterySoC", self.battery_soc)
                self.battery_flag = False
            # if not await self.look_for_roscore():
            #     self.close()
            await asyncio.sleep(0.3)

    async def __sio_callback(self):
        self.async_loop = asyncio.get_running_loop()

        @sio.event
        async def connect(sid, environ):
            self.sio_is_connected = True
            await sio.emit("batterySoC", self.battery_soc)
            await sio.emit("patrolStationName", self.traveling_message)
            await sio.emit("patrolTimer", self.counter)
            self.sid_list.append(sid)
            rospy.loginfo(f'[VUE_UI] Client Connected: {sid}')

        @sio.event
        async def patrolPlay(sid):
            self.gnrl_cmd_pub.publish('play')
            self.is_playing = True
            # await sio.emit("patrolStationName", "Play")

        @sio.event
        async def patrolPause(sid):
            self.gnrl_cmd_pub.publish('pause')
            self.is_playing = False
            # await sio.emit("patrolStationName", "Pause")

        @sio.event
        async def patrolDock(sid):
            self.gnrl_cmd_pub.publish('dock')
            # await sio.emit("patrolStationName", "Dock")

        @sio.event
        async def robotLocalize(sid, data):
            self.gnrl_cmd_pub.publish('localize ' + data)

        @sio.event
        async def sayJoke(sid):
            self.gnrl_cmd_pub.publish('joke')

        @sio.event
        async def saveGoal(sid, data):
            self.gnrl_cmd_pub.publish('saveGoal ' + data)

        @sio.event
        async def removeSavedGoal(sid, data):
            self.gnrl_cmd_pub.publish('removeSavedGoal ' + data)

        @sio.event
        async def addGoalToTask(sid, data):
            self.gnrl_cmd_pub.publish('addGoalToTask ' + data)

        @sio.event
        async def gotoGoal(sid, data):
            self.delivery_goal_pub.publish(data)

        @sio.event
        async def setgoal(sid, data):
            self.gnrl_cmd_pub.publish('setgoal  ' + data)

        @sio.event
        async def cancelGoal(sid):
            self.gnrl_cmd_pub.publish('cancelGoal')

        @sio.event
        async def reloadData(sid):
            await sio.emit("listGoals", self.list_of_goals)
            await sio.emit("patrolTimer", self.counter)
            await sio.emit("patrolStationName", self.traveling_message)

        @sio.event
        async def restartBackend(sid):
            pass

        @sio.event
        def disconnect(sid):
            self.sid_list.remove(sid)
            if not self.sid_list:
                self.sio_is_connected = False
            rospy.loginfo(f'[VUE_UI] Client Disconnected: {sid}')

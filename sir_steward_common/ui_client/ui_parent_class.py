# This is a sample Python script.

# Press Maj+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import rospy
from std_msgs.msg import String

from abc import ABC, abstractmethod


class UiUserClass(ABC):
    """
       This class provides a control on the color of the LED, the state between solid or blinking and the rate
       of the blinking
    """

    def __init__(self, ip_adress: str, port: int, gnrl_cmd_pub: str, status_pub: str):
        # initialising the 3 control variable
        self.ip_adress = ip_adress
        self.port = port
        self.gnrl_cmd_pub = rospy.Publisher(gnrl_cmd_pub, String, queue_size=10)
        self.status_pub = rospy.Publisher(status_pub, String, queue_size=10)

    @abstractmethod
    def connect_ui(self):
        pass

    @abstractmethod
    def close(self):
        pass

#!/usr/bin/env python

from setuptools import setup, find_packages
from catkin_pkg.python_setup import generate_distutils_setup

setup_args = generate_distutils_setup(
    name='sir_steward_common',
    packages=find_packages(),
)
setup(**setup_args)

# setup(
#     version='0.0.0',
#     name='test_drivers',
#     packages=['test_drivers', 'test_drivers.comm_protocols_drivers'],
#     package_dir={
#         'test_drivers': 'test_drivers',
#         'test_drivers.comm_protocols_drivers': 'test_drivers/comm_protocols_drivers'}
# )
# # setup(**d)

